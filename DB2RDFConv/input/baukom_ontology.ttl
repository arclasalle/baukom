@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix dc:	<http://purl.org/dc/elements/1.1/> .
@prefix cpo: <http://www.baukom-catalog.org/baukom/cpo/ontology/cpo.owl#> .
@prefix om: <http://www.wurvoc.org/vocabularies/om-1.8/> .
@prefix clasys: <http://www.baukom-catalog.org/baukom/classificationsystems/ontology/classificationsystems.owl#> .
@base <http://www.baukom-catalog.org/baukom/cpo/ontology/cpo.owl> .

<http://www.baukom-catalog.org/baukom/cpo/ontology/cpo.owl>
	a	owl:Thing ;
	a	owl:Ontology ;
	dc:title	"catalogue of components ontology"@en ;
	dc:creator	"Gonçal Costa" ;
	dc:publisher	"ARC Enginyeria i Arquitectura La Salle"@en ;
	dc:description	"The Baukom ontology provides a description of the domain of structural precast concrete building product data sheets and additional product features."@en ;
	dc:language	"English"@en ;
	dc:format	"OWL Full"@en .

### -------------------------------------------------------------------------------
### Annotation Properties
### -------------------------------------------------------------------------------
	
dc:description 
	rdf:type owl:AnnotationProperty .
	
### -------------------------------------------------------------------------------
### DataType Properties
### -------------------------------------------------------------------------------

dc:date
	rdf:type owl:DatatypeProperty ;
	rdfs:comment "Complete date plus hours and minutes: YYYY-MM-DDThh:mmTZD (eg 1997-07-16T19:20+01:00)" .

cpo:hasName
	rdf:type	owl:DatatypeProperty , owl:FunctionalProperty ;
	rdfs:label	"Has name"@en ;
	rdfs:domain	cpo:Product , cpo:Template , cpo:TemplateObject , cpo:ParameterType , cpo:Company ;
	rdfs:range	xsd:string .	
	
cpo:hasAbbreviation
	rdf:type	owl:DatatypeProperty , owl:FunctionalProperty ;
	rdfs:label	"Has abbreviation"@en ;
	rdfs:domain	cpo:UnitType ;
	rdfs:range	xsd:string .	
	
cpo:hasReferenceCode
	rdf:type owl:DatatypeProperty , owl:FunctionalProperty ;							
	rdfs:label "Has a reference code" ;
	rdfs:domain  cpo:Product ;
	rdfs:range xsd:string .
	
cpo:hasValue
	rdf:type	owl:DatatypeProperty , owl:FunctionalProperty ;
	rdfs:label	"Has value"@en ;
	rdfs:domain	cpo:SimpleValue , cpo:SerieValues ;
	rdfs:range	xsd:string .
	
cpo:hasValueRange1
	rdf:type	owl:DatatypeProperty , owl:FunctionalProperty ;
	rdfs:label	"Has range value 1"@en ;
	rdfs:domain	cpo:RangeValues ;
	rdfs:range	xsd:string .

cpo:hasValueRange2	
	rdf:type	owl:DatatypeProperty , owl:FunctionalProperty ;
	rdfs:label	"Has range value 2"@en ;
	rdfs:domain	cpo:RangeValues ;
	rdfs:range	xsd:string .	

cpo:hasNumVersion
	rdf:type	owl:ObjectProperty ;
	rdfs:label	"Has a number of version"@en ;
	rdfs:domain	cpo:Template ;
	rdfs:range	xsd:int .
	
cpo:hasDesignSystemVersion
	rdf:type	owl:ObjectProperty ;
	rdfs:label	"has a version of the design file"@en ;
	rdfs:domain	cpo:Design ;
	rdfs:range	xsd:string .
	
cpo:hasFileFormat
	rdf:type	owl:DatatypeProperty , owl:FunctionalProperty ;
	rdfs:domain	cpo:Design;
	rdfs:range	[ rdf:type rdfs:Datatype ;
				  owl:oneOf ("revit"^^xsd:string
							 "archicad"^^xsd:string
							 "aecosim"^^xsd:string
							 "allplan"^^xsd:string
							 "ifc"^^xsd:string
							 "other"^^xsd:string )
                ] .

cpo:hasLink
	rdf:type	owl:DatatypeProperty ;
	rdfs:label	"Has a link"@en ;
	rdfs:domain	cpo:Image , cpo:Video , cpo:Document , cpo:Design ;
	rdfs:range	xsd:anyURI .
	
cpo:hasWebSite
	rdf:type	owl:DatatypeProperty ;
	rdfs:label	"Has website"@en ;
	rdfs:domain	cpo:Company ;
	rdfs:range	xsd:anyURI .
	
cpo:hasZipCode
	rdf:type	owl:DatatypeProperty ;
	rdfs:label	"Has zipcode"@en ;
	rdfs:domain	cpo:Company ;
	rdfs:range	xsd:string .
	
cpo:hasGeoLatitude
	rdf:type	owl:DatatypeProperty ;
	rdfs:label	"Has geographic latitude"@en ;
	rdfs:domain	cpo:Company ;
	rdfs:range	xsd:string .
	
cpo:hasGeoLongitude
	rdf:type	owl:DatatypeProperty ;
	rdfs:label	"Has geographic longitude"@en ;
	rdfs:domain	cpo:Company ;
	rdfs:range	xsd:string .
	
cpo:City
	rdf:type	owl:DatatypeProperty ;
	rdfs:label	"city"@en ;
	rdfs:domain	cpo:Company ;
	rdfs:range	xsd:string .
	
	
### -------------------------------------------------------------------------------
### Object Properties
### -------------------------------------------------------------------------------						

dc:creator 
	rdf:type	owl:ObjectProperty .

cpo:hasNextValue
	rdf:type	owl:ObjectProperty ;
	rdfs:label	"Has next value"@en ;
	rdfs:domain	cpo:SerieValues ;
	rdfs:range	cpo:SerieValues .
	
cpo:hasCompany
	rdf:type	owl:ObjectProperty , owl:FunctionalProperty ;
	rdfs:label	"Has a company"@en ;
	rdfs:domain	cpo:Product ;
	rdfs:range	cpo:Company .

cpo:hasCategory				
	rdf:type	owl:ObjectProperty ;
	rdfs:label	"Has category"@en ;	
	rdfs:domain	cpo:Template;
	rdfs:range	clasys:Category .
	
cpo:hasTemplate
	rdf:type	owl:ObjectProperty , owl:FunctionalProperty ;
	rdfs:label	"Has template"@en ;							
	rdfs:domain	cpo:Product ;
	rdfs:range	cpo:Template .

cpo:hasTemplateObject
	rdf:type	owl:ObjectProperty ;
	rdfs:label	"Has template object"@en ;							
	rdfs:domain	cpo:Template ;
	rdfs:range	cpo:TemplateObject .
	
cpo:hasTag
	rdf:type	owl:ObjectProperty ;
	rdfs:label	"Has a tag"@en ;
	rdfs:domain	cpo:Product, cpo:Template ;
	rdfs:range	cpo:Tag .

### Object Properties: Parameters -------------------------------------------------------------						
	
cpo:hasParameterType
	rdf:type	owl:ObjectProperty , owl:FunctionalProperty ;
	rdfs:label	"Has parameter type"@en ;
	rdfs:domain	cpo:Parameter ;
	rdfs:range	cpo:ParameterType .

cpo:hasUnitType
	rdf:type	owl:ObjectProperty , owl:FunctionalProperty ;
	rdfs:label	"Has unit type"@en ;
	rdfs:domain	cpo:ParameterType ;
	rdfs:range	cpo:UnitType .
	
cpo:hasUnit
	rdf:type	owl:ObjectProperty , owl:FunctionalProperty ;
	rdfs:label	"Has unit type"@en ;
	rdfs:domain	cpo:UnitType .
	
cpo:hasParameter
	rdf:type	owl:ObjectProperty ;
	rdfs:label	"Has parameter"@en ;
	rdfs:domain	cpo:ParameterConfigured ;
	rdfs:range	cpo:Parameter .
	
cpo:isParameterOf
	rdf:type	owl:ObjectProperty , owl:FunctionalProperty ;
	rdfs:label	"Is parameter of"@en ;							
	rdfs:domain	cpo:Parameter ;
	rdfs:range	cpo:TemplateObject .

cpo:hasParameterReferenced
	rdf:type	owl:ObjectProperty , owl:FunctionalProperty ;
	rdfs:label	"Has referenced parameter"@en ;
	rdfs:domain	cpo:Parameter ;
	rdfs:range	cpo:Parameter .

cpo:hasParameterParent
	rdf:type	owl:ObjectProperty , owl:FunctionalProperty ;
	rdfs:label	"Has parent parameter"@en ;
	rdfs:domain	cpo:Parameter ;
	rdfs:range	cpo:Parameter .

cpo:hasProductConfiguration
	rdf:type	owl:ObjectProperty ;
	rdfs:label	"Has product configuration"@en ;
	rdfs:domain	cpo:ParameterConfigured ;
	rdfs:range	cpo:ProductConfiguration .
	
cpo:hasValueType
	rdf:type	owl:ObjectProperty , owl:FunctionalProperty ;
	rdfs:label	"Has parameter discipline"@en ;
	rdfs:domain	cpo:ParameterConfigured ;
	rdfs:range	cpo:SimpleValue , cpo:RangeValues , cpo:SerieValues .	

cpo:hasTemplateObjectRef
	rdf:type	owl:ObjectProperty , owl:FunctionalProperty ;
	rdfs:label	"Has template object"@en ;
	rdfs:domain	cpo:ProductConfiguration ;
	rdfs:range	cpo:TemplateObject .	

### Object Properties: Content -------------------------------------------------------------						

cpo:hasContent
	rdf:type	owl:ObjectProperty ;
	rdfs:label	"Has content"@en ;
	rdfs:domain	cpo:Product ;
	rdfs:range	cpo:Content .	
	
cpo:hasDocument
	rdf:type	owl:ObjectProperty ;
	rdfs:label	"Has document"@en ;
	rdfs:domain	cpo:Content ;
	rdfs:range	cpo:Document .	
	
cpo:hasVideo
	rdf:type	owl:ObjectProperty ;
	rdfs:label	"Has video"@en ;
	rdfs:domain	cpo:Content ;
	rdfs:range	cpo:Video .	
	
cpo:hasImage
	rdf:type	owl:ObjectProperty ;
	rdfs:label	"Has image"@en ;
	rdfs:domain	cpo:Content ;
	rdfs:range	cpo:Image .	
	
cpo:hasDesign
	rdf:type	owl:ObjectProperty ;
	rdfs:label	"Has file corresponding to a BIM model of a product"@en ;
	rdfs:domain	cpo:Content ;
	rdfs:range	cpo:Design .
	
### Object Properties: Products ----------------------------------------------------
	
cpo:isCompatibleWith
	rdf:type	owl:ObjectProperty ;
	rdfs:label	"Is a product that is compatible with another product, for example, within a structural system"@en ;
	rdfs:domain	cpo:Product ;
	rdfs:range	cpo:Product .
	
cpo:isSimilarTo
	rdf:type	owl:ObjectProperty ;
	rdfs:label	"Is a product that is similar to another product"@en ;
	rdfs:domain	cpo:Product ;
	rdfs:range	cpo:Product .

cpo:isPartOf
	rdf:type	owl:ObjectProperty ;
	rdfs:label	"Is a product that is part of another product"@en ;
	rdfs:domain	cpo:Product ;
	rdfs:range	cpo:Product .
	
### -------------------------------------------------------------------------------
### Classes
### -------------------------------------------------------------------------------

cpo:Tag				
	rdf:type owl:Class ;
	rdfs:label "Tag"@en .

cpo:Company
	rdf:type owl:Class ;
	rdfs:label "A company"@en .

cpo:Template
	rdf:type owl:Class ;
	rdfs:label "A template"@en .

cpo:TemplateObject	
	rdf:type owl:Class ;
	rdfs:label "Template Object"@en .

cpo:Product
	rdf:type owl:Class ;
	rdfs:label "A product"@en .
	
cpo:ProductConfiguration
	rdf:type owl:Class ;
	rdfs:label "A product Object Configuration"@en .

### Classes / Files -------------------------------------------------------------														

cpo:Content
	rdf:type owl:Class;
	rdfs:label "Content"@en .

cpo:File
	rdf:type owl:Class;
	rdfs:label "File"@en .
	
cpo:Design
	rdf:type owl:Class ;
	rdfs:subClassOf cpo:File ;
	rdfs:label "A design file"@en .

cpo:Image
	rdf:type owl:Class ;
	rdfs:subClassOf cpo:File ;
	rdfs:label "Image"@en .

cpo:Video
	rdf:type owl:Class ;
	rdfs:subClassOf cpo:File ;
	rdfs:label "Video"@en .

cpo:Document
	rdf:type owl:Class ;
	rdfs:subClassOf cpo:File ;
	rdfs:label "Document"@en .
								
### Classes / Parameters -------------------------------------------------------------

cpo:Parameter
	rdf:type owl:Class ;
	rdfs:label "Parameter"@en .																
	
cpo:ParameterConfigured
	rdf:type owl:Class ;
	rdfs:label "Parameter Configured"@en .					
				
cpo:ParameterType
	rdf:type owl:Class ;
	rdfs:label "Parameter Type"@en .

cpo:UnitType
	rdf:type owl:Class ;
	rdfs:label "Unit Type"@en .
	
cpo:SimpleValue
	rdf:type owl:Class ;
	rdfs:label "A simple value"@en .

cpo:RangeValues
	rdf:type owl:Class ;
	rdfs:label "A range of values"@en .

cpo:SerieValues
	rdf:type owl:Class ;
	rdfs:label "A serie of values"@en .


### Ontology of units of Measure (OM) type of units --------------------------------------

om:singular_unit
	rdf:type owl:ObjectProperty .
	
om:dimension
	rdf:type owl:ObjectProperty .

om:Unit_division
	rdf:type owl:Class .

om:Square_metre_multiple_or_submultiple
	rdf:type owl:Class .

om:Cubic_metre_multiple_or_submultiple 
	rdf:type owl:Class .
	
### Ontology of units of Measure (OM) units ----------------------------------------------	

om:millimeter
	rdf:type om:Unit_division .
	
om:gram
	rdf:type om:Singular_unit .
	
om:litre
	rdf:type om:Singular_unit .
	
om:kilogram_per_square_metre 
	rdf:type om:Unit_division .

om:square_millimetre
	rdf:type om:Square_metre_multiple_or_submultiple .

om:degree
	rdf:type om:Singular_unit .

om:cubic_millimetre
	rdf:type om:Cubic_metre_multiple_or_submultiple .

om:newton_per_square_metre
	rdf:type om:Unit_division .

om:metre_kilogram_per_second_time_squared
	rdf:type om:Unit_division .

om:gray
	rdf:type om:Singular_unit .

om:mole
	rdf:type om:Singular_unit .
	
om:radian_per_second-time
	rdf:type om:Unit_division .
	
om:day
	rdf:type om:Singular_unit .
	
om:minute-time
	rdf:type om:Singular_unit .
	
om:newton
	rdf:type om:Singular_unit .
	
om:dynamic_viscosity-dimension
	rdf:type om:Dimension .
	
om:hertz
	rdf:type om:Singular_unit .
	
### Ontology of units of Measure (OM) units ----------------------------------------------		
	
cpo:hasMillimeter
	rdf:type owl:ObjectProperty ;
    rdfs:subPropertyOf cpo:hasUnit .

cpo:hasGram
	rdf:type owl:ObjectProperty ;
    rdfs:subPropertyOf cpo:hasUnit .
	
cpo:hasLitre
	rdf:type owl:ObjectProperty ;
    rdfs:subPropertyOf cpo:hasUnit .
	
cpo:hasKilogram_per_square_metre 
	rdf:type owl:ObjectProperty ;
    rdfs:subPropertyOf cpo:hasUnit .
	
cpo:hasSquare_millimetre
	rdf:type owl:ObjectProperty ;
    rdfs:subPropertyOf cpo:hasUnit .
	
cpo:hasDegree
	rdf:type owl:ObjectProperty ;
    rdfs:subPropertyOf cpo:hasUnit .
	
cpo:hasCubic_millimetre
	rdf:type owl:ObjectProperty ;
    rdfs:subPropertyOf cpo:hasUnit .
	
cpo:hasNewton_per_square_metre
	rdf:type owl:ObjectProperty ;
    rdfs:subPropertyOf cpo:hasUnit .
	
cpo:hasMetre_kilogram_per_second_time_squared
	rdf:type owl:ObjectProperty ;
    rdfs:subPropertyOf cpo:hasUnit .
	
cpo:hasGray
	rdf:type owl:ObjectProperty ;
    rdfs:subPropertyOf cpo:hasUnit .
	
cpo:hasMole
	rdf:type owl:ObjectProperty ;
    rdfs:subPropertyOf cpo:hasUnit .
	
cpo:hasRadian_per_second-time
	rdf:type owl:ObjectProperty ;
    rdfs:subPropertyOf cpo:hasUnit .
	
cpo:hasDay
	rdf:type owl:ObjectProperty ;
    rdfs:subPropertyOf cpo:hasUnit .
	
cpo:hasMinute-time
	rdf:type owl:ObjectProperty ;
    rdfs:subPropertyOf cpo:hasUnit .
	
cpo:hasNewton
	rdf:type owl:ObjectProperty ;
    rdfs:subPropertyOf cpo:hasUnit .
	
cpo:hasDynamic_viscosity-dimension
	rdf:type owl:ObjectProperty ;
    rdfs:subPropertyOf cpo:hasUnit .
	
cpo:hasHertz
	rdf:type owl:ObjectProperty ;
    rdfs:subPropertyOf cpo:hasUnit .
	
### General axioms --------------------------------------------------------------------------------

### General axioms - Restrictions on units properties ---------------------------------------------
	
[ rdf:type owl:Restriction ;
  rdfs:subClassOf om:millimeter ;
  owl:onProperty cpo:hasMillimeter ;
  owl:someValuesFrom owl:Thing
] .

[ rdf:type owl:Restriction ;
  rdfs:subClassOf om:gram ;
  owl:onProperty cpo:hasGram ;
  owl:someValuesFrom owl:Thing
] .

[ rdf:type owl:Restriction ;
  rdfs:subClassOf om:litre ;
  owl:onProperty cpo:hasLitre ;
  owl:someValuesFrom owl:Thing
] .

[ rdf:type owl:Restriction ;
  rdfs:subClassOf om:kilogram_per_square_metre ;
  owl:onProperty cpo:hasKilogram_per_square_metre ;
  owl:someValuesFrom owl:Thing
] .

[ rdf:type owl:Restriction ;
  rdfs:subClassOf om:square_millimetre ;
  owl:onProperty cpo:hasSquare_millimetre ;
  owl:someValuesFrom owl:Thing
] .

[ rdf:type owl:Restriction ;
  rdfs:subClassOf om:degree ;
  owl:onProperty cpo:hasDegree ;
  owl:someValuesFrom owl:Thing
] .

[ rdf:type owl:Restriction ;
  rdfs:subClassOf om:cubic_millimetre ;
  owl:onProperty cpo:hasCubic_millimetre ;
  owl:someValuesFrom owl:Thing
] .

[ rdf:type owl:Restriction ;
  rdfs:subClassOf om:newton_per_square_metre ;
  owl:onProperty cpo:hasNewton_per_square_metre ;
  owl:someValuesFrom owl:Thing
] .

[ rdf:type owl:Restriction ;
  rdfs:subClassOf om:metre_kilogram_per_second_time_squared ;
  owl:onProperty cpo:hasMetre_kilogram_per_second_time_squared ;
  owl:someValuesFrom owl:Thing
] .

[ rdf:type owl:Restriction ;
  rdfs:subClassOf om:gray ;
  owl:onProperty cpo:hasGray;
  owl:someValuesFrom owl:Thing
] .

[ rdf:type owl:Restriction ;
  rdfs:subClassOf om:mole ;
  owl:onProperty cpo:hasMole;
  owl:someValuesFrom owl:Thing
] .
	
[ rdf:type owl:Restriction ;
  rdfs:subClassOf om:radian_per_second ;
  owl:onProperty cpo:hasRadian_per_second ;
  owl:someValuesFrom owl:Thing
] .

[ rdf:type owl:Restriction ;
  rdfs:subClassOf om:day ;
  owl:onProperty cpo:hasDay ;
  owl:someValuesFrom owl:Thing
] .
	
[ rdf:type owl:Restriction ;
  rdfs:subClassOf om:dynamic_viscosity ;
  owl:onProperty cpo:hasDynamic_viscosity-dimension ;
  owl:someValuesFrom owl:Thing
] .

[ rdf:type owl:Restriction ;
  rdfs:subClassOf om:hertz ;
  owl:onProperty cpo:hasHertz ;
  owl:someValuesFrom owl:Thing
] .
