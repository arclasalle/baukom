package url.salle.arc.db2rdf;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.util.FileManager;

import url.salle.arc.db2rdf.controls.InterfaceControl;
import url.salle.arc.db2rdf.controls.LoaderControl;

public class RDFMgr {
	
	// Members:
	private Model moMappingsModel;
	private OntModel moOntModel;
	
	private JPanel moJPSpinnerloader;	
	private String msFilePath;
	
	// Constructor:
	public RDFMgr() {		
	}
	
	public Boolean loadOntology(String asFilePath, JFrame aoFrame, InterfaceControl aoMainPanel) {
		
		if (asFilePath != "") {
			try {
				
				JPanel loJPSpinner = LoaderControl.addSpinner();
				
				Thread t1 = new Thread(new Runnable() {
					public void run() {
						if(moJPSpinnerloader.getComponentCount() == 0){
							moJPSpinnerloader.add(loJPSpinner);
						}
						aoFrame.setEnabled(false);
						aoFrame.invalidate();
						aoFrame.validate();
						aoFrame.repaint();		
					}
				});
				t1.start();				

				Thread t2 = new Thread(new Runnable() {
					public void run() {
						try {
							moOntModel = ModelFactory.createOntologyModel();
							moOntModel.read(asFilePath);		
							msFilePath = asFilePath;
						} catch (Exception ex) {
						}
						
						if(t1 != null){
							t1.interrupt();
						}
						
						moJPSpinnerloader.remove(loJPSpinner);
						moJPSpinnerloader.repaint();
						
						aoFrame.invalidate();
						aoFrame.validate();
						aoFrame.repaint();						
						aoFrame.setEnabled(true);
						
						String lsBaseUri = moOntModel.getNsPrefixURI("");
						aoMainPanel.setOntologyURI(lsBaseUri);
					}
				});
				t2.start();
				
				return true;
			} catch (Exception ex) {
				 System.out.println("Couldn't load the file.");
			}
		}
		return false;
	}
	
	// Methods:
	public Model loadModel(String asFilePath) {
		try {
			return RDFDataMgr.loadModel(asFilePath);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	// Method: the execution of this process generates a TTL mappings file.
	public boolean saveModel(JTextArea aoTFOutput, Model aoModel, String asMappingFilePath) {
		FileOutputStream loFOStream;
		try {
			if (aoModel != null) {
				// The name of the file generated follows this convention:
				// path + filename + "_output.ttl"
				File f = new File(asMappingFilePath);
				loFOStream = new FileOutputStream(f);
				aoModel.write(loFOStream, "TTL");
				loFOStream.close();
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			aoTFOutput.append(e.toString()+"\n");
		}
		return false;
	}	
	
	public boolean addPrefix(String asLocalNamePrefix, String asUrlPrefix){
		if(moMappingsModel != null){
			//Map<String, String> moPrefixesList = moMappingsModel.getNsPrefixMap();
			//for (Map.Entry<String, String> entry : moPrefixesList.entrySet()) {
			//	System.out.println(entry.getValue());
			//}
			// Check if the prefix is already defined:
			String lsPrefix = moMappingsModel.getNsURIPrefix(asUrlPrefix);
			if(lsPrefix == null){
				moMappingsModel.setNsPrefix(asLocalNamePrefix, asUrlPrefix); 
				return true;
			}
		}		
		return false;
	}	
	
	public String getPrefix(Resource aoResource){
		Map<String,String> loPrefixesList = moMappingsModel.getNsPrefixMap();
		String lsNS = normalizeURI(aoResource.getNameSpace()); 
		for (Map.Entry<String, String> entry : loPrefixesList.entrySet()) {
			String lsNSIter = normalizeURI(entry.getValue());
			if(lsNSIter.equals(lsNS)){
				return entry.getKey().toString();
			}
		}
		return "";
	}
	
	public String excludeLocalNameFromURI(String aoURI) {
		if (aoURI.contains("#")) {
			return aoURI.substring(0, aoURI.lastIndexOf('#') + 1);
		} else {
			return aoURI.substring(0, aoURI.lastIndexOf('/') + 1);
		}
	}

	public String normalizeURI(String asURI) {
		if (asURI.substring(asURI.length() - 1).equals("#") ||
			asURI.substring(asURI.length() - 1).equals("/")) {
			return asURI.substring(0, asURI.length() - 1);
		}
		return asURI;
	}
	
	public String getPrefixName(Model aoModel) {
		StmtIterator iter = aoModel.listStatements();
		if(iter.hasNext()){
			Resource loRes = iter.next().getSubject();
			return aoModel.getNsURIPrefix(loRes.getNameSpace());
		}
		return "";
	}
	
	public String getPrefixURI(Model aoModel) {
		StmtIterator iter = aoModel.listStatements();
		if(iter.hasNext()){
			Resource loRes = iter.next().getSubject();
			return loRes.getNameSpace();
		}		
		return "";
	}
	
	public boolean isExistPrefixName(String asPrefixName) {
		Map<String, String> loPrefixesList = moMappingsModel.getNsPrefixMap();
		for (Map.Entry<String, String> entry : loPrefixesList.entrySet()) {
			if(asPrefixName.equals(entry.getKey().toString())){
				return true;
			}
		}
		return false;
	}
	
	public boolean isExistPrefixUri(String asPrefixURI) {
		Map<String, String> loPrefixesList = moMappingsModel.getNsPrefixMap();
		for (Map.Entry<String, String> entry : loPrefixesList.entrySet()) {
			if(asPrefixURI.equals(entry.getValue().toString())){
				return true;
			}
		}
		return false;
	}	
	
	// ------------------------------------------------------------
	
	public void setSpinner(JPanel aoPanel) {
		moJPSpinnerloader = aoPanel;
	}	
	
	public void setMappingsModel(Model aoMappingsModel) {
		moMappingsModel = aoMappingsModel;
	}	
	
	public Model getMappingsModel() {
		return moMappingsModel;
	}	
	
	public boolean isLoaded() {
		if (moMappingsModel != null){
			return true;
		}
		return false;
	}	
}
