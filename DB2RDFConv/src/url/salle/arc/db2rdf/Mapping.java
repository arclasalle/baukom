package url.salle.arc.db2rdf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.FileManager;
import org.apache.jena.vocabulary.RDF;

import url.salle.arc.db2rdf.controls.InterfaceControl;
import url.salle.arc.db2rdf.controls.LoaderControl;
import url.salle.arc.db2rdf.controls.TableControl;

public class Mapping {
	
	// Members:
	private static String msOutputDirectory = "output";
	private static String msMappingFilePath = msOutputDirectory + "/mappings.ttl";
	private static String msD2RQFolder = "d2rq-dev";
	
	private JFrame moJFrame;		
	private JPanel moJPSpinnerloaderGenMapping;
	private JPanel moJPSpinnerloaderGenRDFData;
	private JPanel moJPSpinner;
	
	private JTextArea moTAOutput;
	
	private InterfaceControl moInterface;
	private RDFMgr moRDFMgr;

	// Constructor
	public Mapping(RDFMgr aoRDFMgr) {       
    	moRDFMgr = aoRDFMgr;
	}
	
	// Methods:
	private void generateMappings(boolean abUseR2RML){
		
		System.out.println("extract mappings...");
		moTAOutput.append("extracting mappings..." + "\n");
		
		moJPSpinner = LoaderControl.addSpinner();				
		Thread t1 = new Thread(new Runnable() {
			public void run() {
				if(moJPSpinnerloaderGenMapping.getComponentCount() == 0){
					moJPSpinnerloaderGenMapping.add(moJPSpinner);
				}
				moJFrame.setEnabled(false);
				moJFrame.invalidate();
				moJFrame.validate();
				moJFrame.repaint();		
			}
		});
		t1.start();
		
		Thread t2 = new Thread(new Runnable() {
			public void run() {
				String lsCommand = "";
				// If there are problems:
				// 1. Try to execute the program manually:
				//    > F:\ARC\Programs and Libraries\ARC\DB2RDFConv\d2rq-dev
				//    > generate-mapping --r2rml -o ../output/mappings.ttl -u admin -p xxxxx jdbc:sqlserver://GONS-PC\\GONS:1433;DatabaseName=BaukomTaxonomies

				if(abUseR2RML){
					lsCommand = "cmd /c " + msD2RQFolder + "\\generate-mapping --r2rml -o " + msMappingFilePath + " -u " + Connection.UserName + 
						" -p " + Connection.UserPasword + " " + Connection.Driver + "://" + Connection.Instance + ":" + Connection.Port + ";DatabaseName=" + Connection.DefaultDBName;
				} else {
					lsCommand = "cmd /c " + msD2RQFolder + "\\generate-mapping -o " + msMappingFilePath + " -u " + Connection.UserName + 
						" -p " + Connection.UserPasword + " " + Connection.Driver + "://" + Connection.Instance + ":" + Connection.Port + ";DatabaseName=" + Connection.DefaultDBName;
				}
				moTAOutput.append(lsCommand + "\n");
				moTAOutput.revalidate();
				moTAOutput.repaint();
				if(UtilsApp.ExecuteProc(lsCommand, moTAOutput)) {
					moInterface.loadMappingData();
					System.out.println("done.");
					moTAOutput.append("done." + "\n");
				} else {
					System.out.println("dont with errors.");
				}
				removeSpinnerGenMapping();
			}
		});
		t2.start();
	}	

	// This method extract the mappings from the database selected
	public void extractMappingsConcepts(boolean abUseR2RML) {
		// Note: The result of the extraction is always stored in an internal file path --> "output/mappings.ttl"
		Connection.formatInstanceName();
		generateMappings(abUseR2RML);	
	}
	
	public boolean generateGraphFromDB(String asMappingsFilePath, boolean abUseR2RML) {
		String lsCommand = "";	
		if(abUseR2RML){
			lsCommand = "cmd /c " + msD2RQFolder + "\\generate-mapping --r2rml -o " + asMappingsFilePath + " -v -u "
				+ Connection.UserName + " -p " + Connection.UserPasword + " " + Connection.Driver + "://"
				+ Connection.Instance + ":" + Connection.Port + ";DatabaseName=" + Connection.DefaultDBName;
		} else {
			lsCommand = "cmd /c " + msD2RQFolder + "\\generate-mapping -o " + asMappingsFilePath + " -v -u "
					+ Connection.UserName + " -p " + Connection.UserPasword + " " + Connection.Driver + "://"
					+ Connection.Instance + ":" + Connection.Port + ";DatabaseName=" + Connection.DefaultDBName;
		}
		moTAOutput.append(lsCommand + "\n");
		moTAOutput.revalidate();
		moTAOutput.repaint();
		return UtilsApp.ExecuteProc(lsCommand, moTAOutput);
	}
	
	// Method - generate the graph (ontology) model from the mappings:
	public boolean generateGraphFromMappings(String asMappingsFilePath, Model aoMappingsModel) {
		if(aoMappingsModel != null) {
			String lsContent = "";
			FileOutputStream loFOStream;
			try {
				// Generate a graph using the information in the mappings:
				// Generate Prefixes
				Map<String,String> loPrefixesList = aoMappingsModel.getNsPrefixMap();
				for (Map.Entry<String, String> entry : loPrefixesList.entrySet()) {
					lsContent+= "@prefix " + entry.getKey() +": <" + entry.getValue() + ">\n";
				}
				
				RDFNode loClassMap = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "ClassMap");
				RDFNode loPropertyBridge = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "PropertyBridge");		    	
		    	Property loJoinProp = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "join");
				ResIterator loSubject = aoMappingsModel.listSubjects();
				while (loSubject.hasNext()){
					
					StmtIterator loPropertyBridgeInstance = aoMappingsModel.listStatements(loSubject.next(), RDF.type, loPropertyBridge);
					if(loPropertyBridgeInstance.hasNext()){
						
						Resource loPropSubject = loPropertyBridgeInstance.next().getSubject();						
						StmtIterator loIterJoin = aoMappingsModel.listStatements(loPropSubject, loJoinProp, (RDFNode)null);
						if (loIterJoin.hasNext()) {
							
							  lsContent+=  "\n\trdf:type owl:ObjectProperty ;\n";
							
							// get Domain and Range
							String lsRelation = ((Literal)loIterJoin.next().getObject()).toString();
							String lsDomain = lsRelation.substring(0, lsRelation.indexOf("=>") - 2);
							String lsRange = lsRelation.substring(lsRelation.indexOf("=>") - 2, lsRelation.length()-1);
							lsContent+= "\n\trdfs:domain " + lsDomain + " ;\n";							
							lsContent+= "\n\trdf:range " + lsRange + " ;\n\t.\n";
							
							
							
							// Generate Object properties:
						    
							
							
							//StmtIterator loIterJoin = aoMappingsModel.listStatements(loPropSubject, loJoinProp, (RDFNode)null);
							
							
							
						} else	{
							// Generate Data properties:
						}
					} else {
						StmtIterator loClassMapInstance = aoMappingsModel.listStatements(loSubject.next(), RDF.type, loClassMap);
						if(loClassMapInstance.hasNext()){
						
							
						} else {
							moTAOutput.append("Error: generating the graph from the mappings.\n");
						}
					}
				}
				
				
				// Generate Object classes
				
				
				
				try (PrintWriter out = new PrintWriter(asMappingsFilePath)){
				    out.println(lsContent);
				}
				return true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else{
			moTAOutput.append("No mapping model has been found.");
		}
		return false;
	}
	
	public void generateRDFData(String asMappingsFilePath, String asRDFDataFilePath, boolean abUseR2RML) {
		if ((asMappingsFilePath != "")&&(asRDFDataFilePath!= "")) {
					
			System.out.println("initiating transformation to RDF...");
			moTAOutput.append("initiating transformation to RDF..." + "\n");
		
			JPanel loJPSpinner = LoaderControl.addSpinner();				
			Thread t1 = new Thread(new Runnable() {
				public void run() {
					if(moJPSpinnerloaderGenRDFData.getComponentCount() == 0){
						moJPSpinnerloaderGenRDFData.add(loJPSpinner);
					}
					moJFrame.setEnabled(false);
					moJFrame.invalidate();
					moJFrame.validate();
					moJFrame.repaint();		
				}
			});
			t1.start();				

			Thread t2 = new Thread(new Runnable() {
				public void run() {
					// example: cmd /c d2rq-dev\dump-rdf -u admin -p arc2k3 -f TURTLE -j jdbc:sqlserver://GONS-PC\\GONS:1433;DatabaseName=BaukomTaxonomies mappings.ttl > data.ttl
					String lsCommand = "cmd /c " + msD2RQFolder + "\\dump-rdf -u " + Connection.UserName + " -p " + Connection.UserPasword + " -f TURTLE -j " + 
							Connection.Driver + "://" + Connection.Instance + ":" + Connection.Port + ";DatabaseName=" + Connection.DefaultDBName + " " + 
							"\"" + asMappingsFilePath + "\" > \"" + asRDFDataFilePath + "\"";
					moTAOutput.append(lsCommand + "\n");
					moTAOutput.revalidate();
					moTAOutput.repaint();
					if(UtilsApp.ExecuteProc(lsCommand, moTAOutput)) {
						moInterface.loadMappingData();
						System.out.println("done.");
						moTAOutput.append("done." + "\n");
					} else {
						System.out.println("dont with errors.");
					}
					removeSpinnerGenRDFData();
				}
			});
			t2.start();
		}
	}
	
	private Boolean refineMapping(){
		
		// PART 2:
		// The execution of this process generates a mapping file.
		// The next step is apply the first refinement through loading a rules file.
		// 1. Rename the domain
		// 2. Eliminate D2R Server information
		// X. Save the file as mapping_transf.ttl
		
		String lsContent = "";

		BufferedReader br = null; 
		try {
			br = new BufferedReader(new FileReader(msMappingFilePath));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			lsContent = sb.toString();
			
			// Apply operations:
			// 1. Domain:
			
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			if (br != null) {
				br.close();
			}
		} catch (IOException e) {
			e.printStackTrace();	
			return false;
		}	
		
		PrintWriter out = null;
		try {
			out = new PrintWriter("output\\mapping_transf.ttl");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}
		out.println(lsContent);
		out.close();
		
		return true;
	}
	
	private void removeSpinnerGenMapping() {
		moJPSpinnerloaderGenMapping.remove(moJPSpinner);
		moJPSpinnerloaderGenMapping.repaint();		
		moJFrame.setEnabled(true);
		moJFrame.invalidate();
		moJFrame.validate();
		moJFrame.repaint();		
	}	
	
	private void removeSpinnerGenRDFData() {
		moJPSpinnerloaderGenRDFData.remove(moJPSpinner);
		moJPSpinnerloaderGenRDFData.repaint();		
		moJFrame.setEnabled(true);
		moJFrame.invalidate();
		moJFrame.validate();
		moJFrame.repaint();		
	}	
	
	// --------------------------------------------------------------------
	
	public void setSpinnerGenMapping(JPanel aoPanel) {
		moJPSpinnerloaderGenMapping = aoPanel;
	}
	
	public void setSpinnerGenRDFData(JPanel aoPanel) {
		moJPSpinnerloaderGenRDFData = aoPanel;
	}
	
	public void setFrame(JFrame aoJFrame) {
		moJFrame = aoJFrame;
	}
	
	public void setInterface(InterfaceControl aoInterface) {
		moInterface = aoInterface;
	}
	
	public void setOutPut(JTextArea aoTAOutput) {
		moTAOutput = aoTAOutput;
	}
}
