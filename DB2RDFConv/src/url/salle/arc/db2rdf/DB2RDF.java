package url.salle.arc.db2rdf;

import java.awt.Dimension;
import java.awt.MenuBar;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import url.salle.arc.db2rdf.controls.InterfaceControl;
import url.salle.arc.db2rdf.controls.LoaderControl;
import url.salle.arc.db2rdf.controls.MenuBarControl;

public class DB2RDF extends JFrame {
	
	/*
	 * Description:
	 * 
	 * This application enable two main goals: 
	 * 1. Extends a target ontology with the part of the schema corresponding to a new data source to be included.
	 * 2. The transformation of the data from this source (database) according to the corresponding representation in the ontology.       
	 * 
	 * This application do not provide the matching between instances of the data sources and existing data included in the target domain.  
	 * This is performed in through adapting the platform to allow manufacturers define the specific relations.  
	 * 
	 */	
	
	//Optionally set the look and feel.
    private static boolean useSystemLookAndFeel = false;
   
	public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	createAndShowGUI();
            }
        });
    }
	
	/**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event dispatch thread.
     */
    private static void createAndShowGUI() {
    	
        if (useSystemLookAndFeel) {
            try {
                UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
            } catch (Exception e) {
                System.err.println("Couldn't use system look and feel.");
            }
        }
        
        MenuBarControl loMenuBar = new MenuBarControl();
        RDFMgr moRDFMgr = new RDFMgr();
        
        InterfaceControl loInterfaceControl = new InterfaceControl(moRDFMgr);
        
        //Create and set up the window.
        JFrame frame = new JFrame("DB2RDF Converter");
        frame.setJMenuBar(loMenuBar.get());
        frame.add(loInterfaceControl.getMainContainerObject());
        frame.getContentPane().validate();
        frame.getContentPane().repaint();        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
		frame.setVisible(true);
		
		loMenuBar.create(moRDFMgr, frame, loInterfaceControl);
		loInterfaceControl.setFrame(frame);
    }  
}