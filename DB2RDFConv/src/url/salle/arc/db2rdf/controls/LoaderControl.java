package url.salle.arc.db2rdf.controls;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

public class LoaderControl {

	public static JPanel addSpinner() {
		JPanel loJPanel = new JPanel();
		ImageIcon loading = new ImageIcon("img/ajax-loader.gif");
		loJPanel.add(new JLabel("loading... ", loading, JLabel.CENTER));
		Dimension d = loJPanel.getPreferredSize();
		d.height = Global.BUTTON_HEIGHT;
		loJPanel.setPreferredSize(d);   		
		return loJPanel;
	}
	
	// Method -
	public static JPanel create() {

		JPanel loJPanel = new JPanel();

		JButton b1 = new JButton("one");
		JButton b2 = new JButton("two");
		JButton b3 = new JButton("three");

		loJPanel.add(b1);
		loJPanel.add(b2);
		loJPanel.add(b3);

		Insets insets = loJPanel.getInsets();
		Dimension size = b1.getPreferredSize();

		b1.setBounds(25 + insets.left, 5 + insets.top, size.width, size.height);
		size = b2.getPreferredSize();
		b2.setBounds(55 + insets.left, 40 + insets.top, size.width, size.height);
		size = b3.getPreferredSize();
		b3.setBounds(150 + insets.left, 15 + insets.top, size.width + 50, size.height + 20);

		return loJPanel;
	}
}
