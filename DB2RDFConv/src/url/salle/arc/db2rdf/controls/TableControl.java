package url.salle.arc.db2rdf.controls;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.apache.commons.lang3.tuple.Triple;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.ResourceUtils;

import url.salle.arc.db2rdf.RDFMgr;

public class TableControl {
	
	// Members:
	private JTable moMappingsTable;	
	private JTable moMappingsTableObjProps;
	private JTable moPrefixes;
	private Object[][] moRDFData;
	private Object[][] moRDFDataObjProps;
	private Object[] moRDFPrefixes;
	private JTextArea moTAOutput;
	private Map<String,String> moPrefixesList;
	private RDFMgr moRDFMgr;
	
	// Constructor:
    public TableControl(RDFMgr aoRDFMgr) {       
    	moRDFMgr = aoRDFMgr;
    }

    // Methods:
    public void setEmptyTable() {
    	
    	Object[] columnNames = {"Selected", "LName", "Classes", "Selected", "LName", "Data properties"};
    	Object[][] data = new Object[0][6];
    	
    	DefaultTableModel model = new DefaultTableModel(data, columnNames);
		moMappingsTable = new JTable(model) { };
        moMappingsTable.setFillsViewportHeight(true);
        
        Object[] columnNamesObjectProps = {"Selected", "Domain", "LName", "Object properties", "Range"};
    	Object[][] dataObjectProps = new Object[0][5];

    	DefaultTableModel loModelObjectProps = new DefaultTableModel(dataObjectProps, columnNamesObjectProps);
    	moMappingsTableObjProps = new JTable(loModelObjectProps) { };
    	moMappingsTableObjProps.setFillsViewportHeight(true);

    	Object[] columnNamesPrefixes = {"Selected", "LName", "URI" };
    	Object[][] dataPrefixes = new Object[0][3];

    	DefaultTableModel loModelPrefixes = new DefaultTableModel(dataPrefixes, columnNamesPrefixes);
    	moPrefixes = new JTable(loModelPrefixes) { };
    	moPrefixes.setFillsViewportHeight(true);
    	
    	formatTableColumnsMappingTable();
    	formatTableColumnsMappingTableObjectProps();
    	formatTableColumnsPrefixes();
    }

    public void createMappingTableR2RML(Model aoMappingsModel) {
    	// define reusable concepts:
    	Property loSubjectMap = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("rr"), "subjectMap");
    	Property loClass = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("rr"), "class");
    	Property loPredicate = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("rr"), "predicate");
    	Property loObjectMap = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("rr"), "objectMap");
    	Property loParentTriplesMap = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("rr"), "parentTriplesMap");
    	Property loPredicateObjectMap = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("rr"), "predicateObjectMap");
    	// get prefixes:
    	Object[][] la2DTablePrefixes = getPrefixes(aoMappingsModel);
    	List<Object[]> lo2DTableData = new ArrayList<Object[]>();
    	List<Object[]> lo2DTableDataObjProps = new ArrayList<Object[]>();
    	List<Object[]> loRDFData = new ArrayList<Object[]>();
    	List<Object[]> loRDFDataObjProps = new ArrayList<Object[]>();
    	// Iterate classes
    	int liNumDP = 0;
    	int liNumDataPropertiesTemp = 0;
    	ResIterator loIterClasses = aoMappingsModel.listSubjects();
    	// for each class...
    	while (loIterClasses.hasNext()){   		
    		Resource loClasse = loIterClasses.next();
    		liNumDataPropertiesTemp = 0;
    		
    		// DTableData --> store the data to be visualized in the interface (names of the concepts) 
    		// moRDFData  --> store the RDF objects corresponding to each concept  
    		
    		Object[] loRDFDataRow = new Object[2];
    		loRDFDataRow[0] = loClasse; 
    		liNumDP = 0;
    		
    		StmtIterator loIterSubjectMap = aoMappingsModel.listStatements(loClasse, loSubjectMap, (RDFNode)null);
    		while (loIterSubjectMap.hasNext()){
    			RDFNode loBlankNodeSubjectMap = loIterSubjectMap.next().getObject();
    			if(loBlankNodeSubjectMap != null){
    				StmtIterator loIterClass = aoMappingsModel.listStatements((Resource)loBlankNodeSubjectMap, loClass, (RDFNode)null);
    				if (loIterClass.hasNext()){
    					Resource loClasseInstance = (Resource)loIterClass.next().getObject();
    					
    					Object[] loTableDataRow = new Object[6];
    					loTableDataRow[0] = new Boolean(true);
    					loTableDataRow[1] = moRDFMgr.getPrefix(loClasseInstance);
    					loTableDataRow[2] = loClasseInstance.getLocalName();
			    		
    					StmtIterator loIterPredicateObjectMap = aoMappingsModel.listStatements(loClasse, loPredicateObjectMap, (RDFNode)null);
			    		while (loIterPredicateObjectMap.hasNext()){
			    			RDFNode loBlankNodePredicateObjectMap = loIterPredicateObjectMap.next().getObject();
			    			if(loBlankNodePredicateObjectMap != null){
			    				StmtIterator loIterTriplesMaps = aoMappingsModel.listStatements((Resource)loBlankNodePredicateObjectMap, loObjectMap, (RDFNode)null);
			    				StmtIterator loIterPredicate = aoMappingsModel.listStatements((Resource)loBlankNodePredicateObjectMap, loPredicate, (RDFNode)null);
			    				if (loIterPredicate.hasNext()){
			    					Statement loStatTargetConceptTriple = loIterPredicate.next();
			    					Resource loTargetConceptName = (Resource)loStatTargetConceptTriple.getObject();
			    					//System.out.println(">>> " + loTargetConceptName.getLocalName());
				    				if (loIterTriplesMaps.hasNext()){
				    					RDFNode loBlankNodeObjectMap = loIterTriplesMaps.next().getObject();
				    					if(loBlankNodeObjectMap != null){
				    						StmtIterator loIterParentTriplesMap = aoMappingsModel.listStatements((Resource)loBlankNodeObjectMap, loParentTriplesMap, (RDFNode)null);
				    						if(loIterParentTriplesMap.hasNext()) {
				    							// Object property:
				    							// get domain
				    							Resource loDomainClass = (Resource)loIterParentTriplesMap.next().getObject();
												
												Object[] loTableDataObjPropsRow = new Object[5];
												loTableDataObjPropsRow[0] = new Boolean(true);
												loTableDataObjPropsRow[1] = loDomainClass.getLocalName(); // Domain
												loTableDataObjPropsRow[2] = moRDFMgr.getPrefix(loTargetConceptName);
												loTableDataObjPropsRow[3] = loTargetConceptName.getLocalName();
												loTableDataObjPropsRow[4] = loClasseInstance.getLocalName(); // Range
												lo2DTableDataObjProps.add(loTableDataObjPropsRow);
												
												Object[] loRDFDataObjPropsRow = new Object[3];
												loRDFDataObjPropsRow[0] = loDomainClass;
												loRDFDataObjPropsRow[1] = loTargetConceptName;
												loRDFDataObjPropsRow[2] = loClasseInstance;
												loRDFDataObjProps.add(loRDFDataObjPropsRow);
					    						
				    						} else {
				    							// Data property:
				    							liNumDataPropertiesTemp++;			    							
				    							
				    							if (liNumDP > 0) {
					    							loTableDataRow = new Object[6];
					    							loTableDataRow[0] = new Boolean(false);
													loTableDataRow[1] = null;
													loTableDataRow[2] = "";
					    	    				
													loRDFDataRow = new Object[2];												
					    	    					loRDFDataRow[0] = null;
				    							}				    							
							    				
				    							loTableDataRow[3] = new Boolean(true);
				    							loTableDataRow[4] = moRDFMgr.getPrefix((Resource)loTargetConceptName);
				    							loTableDataRow[5] = ((Resource)loTargetConceptName).getLocalName(); // name of the final class
				    							lo2DTableData.add(loTableDataRow);
				    							
				    							loRDFDataRow[1] = loStatTargetConceptTriple;	
				    							loRDFData.add(loRDFDataRow);
				    							
				    							liNumDP++;
				    						}
				    					}
				    				}
			    				}    				
			    			} 			
			    		}
			    		if(liNumDataPropertiesTemp == 0){
			    			// the class has no data properties:
			    			loTableDataRow[3] = Boolean.FALSE;
							loTableDataRow[4] = null;
							loTableDataRow[5] = "";
							lo2DTableData.add(loTableDataRow);
			    			
							loRDFDataRow[1] = null;
							loRDFData.add(loRDFDataRow);
			    		}
    				}
    			}
    		}
    	}
    	
    	Object[][] la2DTableData = new Object[lo2DTableData.size()][6];
    	for (int i = 0; i < lo2DTableData.size(); i++) {
    		la2DTableData[i] = lo2DTableData.get(i);
    	}
    	
    	Object[][] la2DTableDataObjProps = new Object[lo2DTableDataObjProps.size()][6];
    	for (int i = 0; i < lo2DTableDataObjProps.size(); i++) {
    		la2DTableDataObjProps[i] = lo2DTableDataObjProps.get(i);
    	}
    	
    	moRDFData = new Object[loRDFData.size()][2];   	
    	for (int i = 0; i < loRDFData.size(); i++) {
    		moRDFData[i] = loRDFData.get(i);
    	}
    	
    	moRDFDataObjProps = new Object[loRDFDataObjProps.size()][3];
    	for (int i = 0; i < loRDFDataObjProps.size(); i++) {
    		moRDFDataObjProps[i] = loRDFDataObjProps.get(i);
    	}
    	
    	loadTable(la2DTableData);
		loadTableObjectProps(la2DTableDataObjProps);
		loadTablePrefixes(la2DTablePrefixes);
    }
    
    public void createMappingTableD2RQ(Model aoMappingsModel) {    	
    	// define reusable concepts:
    	Property loJoinProp = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "join");
    	Property loBelongsToClassMap = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "belongsToClassMap");
    	Property loD2rqClass = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "class");
    	Property loD2rqProperty = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "property");
    	Property loRefersToClassMap = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "refersToClassMap"); // range
    	RDFNode loClassMap = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "ClassMap");
    	// get prefixes: 
    	Object[][] la2DTablePrefixes = getPrefixes(aoMappingsModel);
    	// get object properties
    	int liNumMaxRows = 0;
    	int liNumObjectProperties = 0;
		StmtIterator loDPs = aoMappingsModel.listStatements((Resource)null, loBelongsToClassMap, (RDFNode)null);
		while (loDPs.hasNext()){
			// e.g. [ map:dbo_CAT_Categories_deleted  <<d2rq:belongsToClassMap>>  map:dbo_CAT_Categories ]
			// Discard data object properties for this table:
			StmtIterator loIterJoin = aoMappingsModel.listStatements(loDPs.next().getSubject(), loJoinProp, (RDFNode)null);
			if (!loIterJoin.hasNext()) {
				liNumMaxRows++;
			} else {
				liNumObjectProperties++;
			}
		}
		
		// If there are classes with no Data properties, a row have to be added:
		StmtIterator loIterClassInstances = aoMappingsModel.listStatements((Resource)null, null, loClassMap);
		while (loIterClassInstances.hasNext()) {
			StmtIterator loIterOP = aoMappingsModel.listStatements(null, loBelongsToClassMap, loIterClassInstances.next().getSubject());
			if (!loIterOP.hasNext()) {
				liNumMaxRows++; // No properties
			} else {
				// example case 1: has 1 object prop and 1 data prop (nothing to do)
				// example case 2: has 2 object props (add 1 row)
				// find a property that is not an object property:
				while (loIterOP.hasNext()){
					StmtIterator loIterProps = aoMappingsModel.listStatements(loIterOP.next().getSubject(), loJoinProp, (RDFNode)null);
					if(!loIterProps.hasNext()) {
						liNumMaxRows--;
						break; // Data Property
					}
				}
				liNumMaxRows++;
			}
		}		
    	
    	// Classes and Data properties data: 
		Object[][] la2DTableData = new Object[liNumMaxRows][4];
		moRDFData = new Object[liNumMaxRows][2];

		// Object properties data
		Object[][] la2DTableDataObjProps = new Object[liNumObjectProperties][5];
    	moRDFDataObjProps = new Object[liNumObjectProperties][3];
		
		// Get classes:
		loIterClassInstances = aoMappingsModel.listStatements((Resource)null, null, loClassMap);
		int liNumRow = 0;
		while (loIterClassInstances.hasNext())  
		{
			// e.g.: [ <<map:dbo_CAT_Categories>>   rdf:type   d2rq:ClassMap ]
			Resource loClassMappingName = loIterClassInstances.next().getSubject();
			StmtIterator loIter1 = aoMappingsModel.listStatements(loClassMappingName, loD2rqClass, (RDFNode)null);
			if (loIter1.hasNext()) {
				// [ map:dbo_CAT_Categories d2rq:class <<vocab:dbo_CAT_Categories>> ]
				Resource loFinalClassIntanceName = (Resource)(loIter1.next()).getObject();  
			
				Object[] loTableDataRow = new Object[6];
				loTableDataRow[0] = new Boolean(true);
				loTableDataRow[1] = moRDFMgr.getPrefix(loFinalClassIntanceName);
				loTableDataRow[2] = loFinalClassIntanceName.getLocalName();
				
				Object[] loRDFDataRow = new Object[2];
				loRDFDataRow[0] = loClassMappingName;  // name of the mapping used to transform the data to the final class
				
				// Properties --------------------------------------------------------------------------------------------------------
				// e.g.: [ map:dbo_CAT_Categories_description  d2rq:belongsToClassMap  map:dbo_CAT_Categories]
				StmtIterator loIter2 = aoMappingsModel.listStatements(null, loBelongsToClassMap, loClassMappingName);
				int liNumDP = 0;
				int liNumOP = 0;
				while (loIter2.hasNext()) {
					// Check for data property or object property:
					Resource loDataProperty_or_ObjectProperty_MappingName = loIter2.next().getSubject();
					StmtIterator loIter3 = aoMappingsModel.listStatements(loDataProperty_or_ObjectProperty_MappingName, loD2rqProperty, (RDFNode)null);
					if (loIter3.hasNext()) {
						Resource loFinalDataPropery_or_ObjectProperty_IntanceName = (Resource)(loIter3.next()).getObject();
						StmtIterator loIter4 = aoMappingsModel.listStatements(loDataProperty_or_ObjectProperty_MappingName, loJoinProp, (RDFNode)null);
						if (loIter4.hasNext()) {
							
							// Object Property:							
							StmtIterator loIterDomain = aoMappingsModel.listStatements(loDataProperty_or_ObjectProperty_MappingName, loBelongsToClassMap, (RDFNode)null);
							if (loIterDomain.hasNext()) {
								Resource loDomainClass = (Resource)loIterDomain.next().getObject();
								StmtIterator loIterRange = aoMappingsModel.listStatements(loDataProperty_or_ObjectProperty_MappingName, loRefersToClassMap, (RDFNode)null);
								if (loIterRange.hasNext()) {
									
									// example:
									// domain >> [ map:dbo_CAT_Categories_fkTaxonomySystem__ref  d2rq:belongsToClassMap  map:dbo_CAT_Categories ] 
								    // range  >> [ map:dbo_CAT_Categories_fkTaxonomySystem__ref  d2rq:refersToClassMap   map:dbo_CAT_TaxonomySystems ]

									RDFNode loRange = loIterRange.next().getObject();
									moRDFDataObjProps[liNumOP] = setObjectPropertyTableData(aoMappingsModel, loFinalDataPropery_or_ObjectProperty_IntanceName, loDomainClass, loRange);
																		
									Object[] loTableDataObjPropsRow = new Object[5];
									loTableDataObjPropsRow[0] = new Boolean(true);
									loTableDataObjPropsRow[1] = ((Resource)moRDFDataObjProps[liNumOP][0]).getLocalName();
									loTableDataObjPropsRow[2] = moRDFMgr.getPrefix(loFinalDataPropery_or_ObjectProperty_IntanceName);
									loTableDataObjPropsRow[3] = loFinalDataPropery_or_ObjectProperty_IntanceName.getLocalName();
									if(moRDFDataObjProps[liNumOP][2] instanceof  Resource) {
										loTableDataObjPropsRow[4] = ((Resource)moRDFDataObjProps[liNumOP][2]).getLocalName();
									} else {
										loTableDataObjPropsRow[4] = moRDFDataObjProps[liNumOP][2].toString();
									}
									la2DTableDataObjProps[liNumOP] = loTableDataObjPropsRow;
									liNumOP++;
								}
							}
						} else {
							
							// Data property:
							if (liNumDP > 0) {
								// Add class empty slot:
								loTableDataRow = new Object[6];
								loTableDataRow[0] = new Boolean(false);
								loTableDataRow[1] = null;
								loTableDataRow[2] = "";
								
								loRDFDataRow = new Object[2];
								loRDFDataRow[0] = null;
							}
							
							loTableDataRow[3] = new Boolean(true);
							loTableDataRow[4] = moRDFMgr.getPrefix(loFinalDataPropery_or_ObjectProperty_IntanceName);
							loTableDataRow[5] = loFinalDataPropery_or_ObjectProperty_IntanceName.getLocalName();// name of the final class					
							loRDFDataRow[1] = loDataProperty_or_ObjectProperty_MappingName;			 		// name of the mapping used to transform the data to the final property
							
							la2DTableData[liNumRow] = loTableDataRow;
							moRDFData[liNumRow] = loRDFDataRow;
							
							liNumRow++;
							liNumDP++;
						}
					} else {
						moTAOutput.append("Error: the name of the property (data or object property) " + loDataProperty_or_ObjectProperty_MappingName.getLocalName() + " is not valid\n");
					}
				} 
				
				if(liNumDP == 0){
					// the class has not data properties (Perhaps object properties)
					loTableDataRow[3] = Boolean.FALSE;
					loTableDataRow[4] = null;
					loTableDataRow[5] = "";
					la2DTableData[liNumRow] = loTableDataRow;
					
					// the data property is null:
					loRDFDataRow[1] = null;
					moRDFData[liNumRow] = loRDFDataRow; 
					liNumRow++;
				}
			}
		}		
		loadTable(la2DTableData);
		loadTableObjectProps(la2DTableDataObjProps);
		loadTablePrefixes(la2DTablePrefixes);
	}  
    
    private void loadTable(Object[][] aoData){
    	Object[] columnNames = {"Selected", "LName", "Classes", "Selected", "LName", "Data Properties"};
    	DefaultTableModel model = new DefaultTableModel(aoData, columnNames);
		moMappingsTable = new JTable(model) {

            private static final long serialVersionUID = 1L;

            @Override
            public boolean isCellEditable(int row, int column){
            	// Compare to original values, not the current ones:            	
            	if(aoData[row][column] instanceof String) {
	            	if(aoData[row][column].equals("")) {
	            		return false;
	            	}
	            }
            	else if(aoData[row][column] instanceof Boolean) {
            		if(aoData[row][column+2].equals("")){
            			return false;
            		}
            	}
           		return true;
            }
            
            @Override
            public Class getColumnClass(int column) {
                switch (column) {
                    case 0: return Boolean.class;
                    case 1: return JComboBox.class;
                    case 2: return String.class;
                    case 3: return Boolean.class;                                            
                    case 4: return JComboBox.class;
                    case 5: return String.class;
                    default: return String.class;
                }
            }
        };
        
        moMappingsTable.setFillsViewportHeight(true);
        formatTableColumnsMappingTable();
        
        // Fill the ComboBox columns:
        setUpSportColumn(moMappingsTable, moMappingsTable.getColumnModel().getColumn(1));
        setUpSportColumn(moMappingsTable, moMappingsTable.getColumnModel().getColumn(4));
    }
    
    private void loadTableObjectProps(Object[][] aoData){
    	Object[] columnNames = {"Selected", "Domain", "LName", "Object properties", "Range"};
    	DefaultTableModel model = new DefaultTableModel(aoData, columnNames);
    	moMappingsTableObjProps = new JTable(model) {

            private static final long serialVersionUID = 1L;

            @Override
			public boolean isCellEditable(int row, int column) {
				if (column == 0 || column == 2 || column == 3) {
					return true;
				} else {
					return false;
				}
			}
            
            @Override
            public Class getColumnClass(int column) {
                switch (column) {
                    case 0: return Boolean.class;
                    case 1: return String.class;
                    case 2: return JComboBox.class;
                    case 3: return String.class;
                    case 4: return String.class;                                            
                    default: return String.class;
                }
            }
        };
        
        moMappingsTableObjProps.setFillsViewportHeight(true);
        formatTableColumnsMappingTableObjectProps();
        
        // Fill the ComboBox column:
        setUpSportColumn(moMappingsTableObjProps, moMappingsTableObjProps.getColumnModel().getColumn(2));
    }
    
    private void loadTablePrefixes(Object[][] aoData){
    	Object[] columnNames = {"Selected", "LName", "URI" };
    	DefaultTableModel model = new DefaultTableModel(aoData, columnNames);
    	moPrefixes = new JTable(model) {
    		
    		private static final long serialVersionUID = 1L;
    		
    		@Override
            public boolean isCellEditable(int row, int column){
            	return false;
            }
    		
    		 @Override
             public Class getColumnClass(int column) {
                 switch (column) {
                     case 0: return Boolean.class;
                     case 1: return String.class;
                     case 2: return String.class;                                                                 
                     default: return String.class;
                 }
             }
    	};    	
    	
    	moPrefixes.setFillsViewportHeight(true);
    	formatTableColumnsPrefixes();
    }
    
	private void formatTableColumnsMappingTable() {
    	 TableColumn column = null;
         for (int i = 0; i < 6; i++) {
             column = moMappingsTable.getColumnModel().getColumn(i);
             if ((i == 0)||(i == 3)) {
                 column.setPreferredWidth(10);               
             } else if ((i == 1)||(i == 4)) { 
            	 column.setPreferredWidth(60);
             } else {
                 column.setPreferredWidth(400);
             }
         }
    }
	
	private void formatTableColumnsMappingTableObjectProps() {
		TableColumn column = null;
		for (int i = 0; i < 5; i++) {
			column = moMappingsTableObjProps.getColumnModel().getColumn(i);
			if (i == 0) {
				column.setPreferredWidth(10);
			} else if (i == 2) {
				column.setPreferredWidth(60);
			} else {
				column.setPreferredWidth(400);
			}
		}
	}

	private void formatTableColumnsPrefixes() {
		TableColumn column = null;
		for (int i = 0; i < 3; i++) {
			column = moPrefixes.getColumnModel().getColumn(i);
			if (i == 0) {
				column.setPreferredWidth(10);
			} else if (i == 1) {
				column.setPreferredWidth(50);
			} else if (i == 2) {
				column.setPreferredWidth(800);
			}
		}
	}
	
	// PREFIXES
	private Object[][] getPrefixes(Model aoMappingsModel){
		moPrefixesList = aoMappingsModel.getNsPrefixMap();
		Object[][] loPrefixes2D = new Object[moPrefixesList.size()][3]; 
		int i=0;
		for (Map.Entry<String, String> entry : moPrefixesList.entrySet()) {
		    //System.out.println(entry.getKey() + " " + entry.getValue());
		    Object[] loPrefixes2DRow = new Object[3];
		    loPrefixes2DRow[0] = new Boolean(true);
		    loPrefixes2DRow[1] = entry.getKey() ;
		    loPrefixes2DRow[2] = entry.getValue();
		    loPrefixes2D [i] = loPrefixes2DRow;
    		i++;
		}
		return loPrefixes2D;		
	}
	
	public void setUpSportColumn(JTable aoTable, TableColumn aoSportColumn) {
	
		// Set up the editor for the sport cells.
        JComboBox comboBox = new JComboBox();
		for (Map.Entry<String, String> entry : moPrefixesList.entrySet()) {
			comboBox.addItem(entry.getKey());
		}
        aoSportColumn.setCellEditor(new DefaultCellEditor(comboBox));

        // Set up tool tips for the sport cells:
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        renderer.setToolTipText("Click for combo box");
        aoSportColumn.setCellRenderer(renderer);
	}
	
	// Method: finds the  object property relation
	private Object[] setObjectPropertyTableData(Model aoMappingsModel, Resource aoObjectProperty, Resource aoDomainClass, RDFNode aoRange) {
		Object[] loRDFDataObjPropsRow = new Object[3];
		loRDFDataObjPropsRow[1] = aoObjectProperty;
		try {
			// find the final class names of the domain:
			Property loD2rqClass = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "class");	
			StmtIterator loIter1 = aoMappingsModel.listStatements(aoDomainClass, loD2rqClass, (RDFNode)null);
			if (loIter1.hasNext()) {
				// [ map:dbo_CAT_Categories d2rq:class <<vocab:dbo_CAT_Categories>> ]
				Resource loFinalClassDomainName = (Resource)(loIter1.next()).getObject();
				loRDFDataObjPropsRow[0] = loFinalClassDomainName;
				
				Resource loFinalClassRangeName = null;
				if (aoRange.isURIResource()) {
					// find the final class names of the range:
					StmtIterator loIter2 = aoMappingsModel.listStatements((Resource)aoRange, loD2rqClass, (RDFNode)null);
					if (loIter2.hasNext()) {
						// [ map:dbo_CAT_Categories d2rq:class <<vocab:dbo_CAT_Categories>> ]
						loFinalClassRangeName = (Resource)(loIter2.next()).getObject();
						loRDFDataObjPropsRow[2] = loFinalClassRangeName;
					} else {
						moTAOutput.append("Error: the name of the object " + ((Resource)aoRange).getLocalName()
						+ " (the range class) does not exist in the mapping file\n");
					}
				} else if (aoRange.isLiteral()) {
					loRDFDataObjPropsRow[2] = ((Literal)aoRange).getValue();
				} else {
					moTAOutput.append("Error: the name of the object " + aoObjectProperty.getLocalName()
							+ " is has not been identified\n");
				}
				return loRDFDataObjPropsRow;
			}
		}
		catch (Exception e) {
			moTAOutput.append(
					"Error: creating information for object property " + aoObjectProperty.getLocalName() + "\n");
		}
		return null;
	}
	
	// Method: Update the R2RML mappings RDF model
	public void updateR2RMLMappingsFromTable(Model aoMappingsModel) {
		// Important: table is always regenerated from the current RDF mapping model!
		ArrayList<Statement> llstToDelete = new ArrayList<Statement>();
		Property loPredicate = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("rr"), "predicate");
		Property loPredicateObjectMap = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("rr"), "predicateObjectMap");
    	Property loObjectMap = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("rr"), "objectMap");
    	Property loParentTriplesMap = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("rr"), "parentTriplesMap");
    	Property loSubjectMap = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("rr"), "subjectMap");
    	Property loClass = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("rr"), "class");
		for (int liRow = 0; liRow < moMappingsTable.getRowCount(); ) {
			// classes:
			String lsClassNameTableValue = (String) moMappingsTable.getModel().getValueAt(liRow, 2);
			if (!lsClassNameTableValue.equals("")) {
				// get the Resource:
				Resource loClassMapping = (Resource) moRDFData[liRow][0];
				if(!loClassMapping.equals(null)) {
					Boolean loJChecked = (Boolean) moMappingsTable.getModel().getValueAt(liRow, 0);
					if (!loJChecked) {
						// --> DELETE the CLASS and their data and object properties have to be deleted -------------------
						StmtIterator loTriplesToDelete = aoMappingsModel.listStatements((Resource)loClassMapping, null, (RDFNode)null);
						while (loTriplesToDelete.hasNext()) {
							llstToDelete.add(loTriplesToDelete.next());
						}
				    	aoMappingsModel.remove(llstToDelete);
					} else {
						// LOCAL NAME of the class has changed? ------------------------------------------------------------
						// Retrieve the name of the final class:
						StmtIterator loIterClassMapping = aoMappingsModel.listStatements((Resource)loClassMapping, loSubjectMap, (RDFNode)null);
						if (loIterClassMapping.hasNext()) {
							RDFNode loBlankNodeSubjectMap = loIterClassMapping.next().getObject();
			    			if(loBlankNodeSubjectMap != null){
			    				StmtIterator loIterClass = aoMappingsModel.listStatements((Resource)loBlankNodeSubjectMap, loClass, (RDFNode)null);
			    				if (loIterClass.hasNext()){
			    					// Review COMBOBOX:
			    					Statement loStateClass = loIterClass.next();
			    					Resource loClasseInstance = (Resource)loStateClass.getObject();			    				
			    					if(!lsClassNameTableValue.equals(loClasseInstance.getLocalName())){
				    					if(verifyLocalName(lsClassNameTableValue)) {
					    					String loNewURI = loClasseInstance.getNameSpace() + lsClassNameTableValue;
											ResourceUtils.renameResource(loClasseInstance, loNewURI);
											// Is not necessary update object properties --> the name of the mapping remains --> BUT, CHECK consistency
				    					} else {
				    						moTAOutput.append("Error: the name of the class " + lsClassNameTableValue + " is not valid\n");
				    						// Model is not updated.
				    					}
			    					}
			    					// check the prefix (ComboBox)
			    					String lsNameSpace = aoMappingsModel.getNsPrefixURI((String) moMappingsTable.getModel().getValueAt(liRow, 1));
									if(!lsNameSpace.equals(loClasseInstance.getNameSpace())){
										// Create a new resource:
										//System.out.println("1:" + lsNameSpace);
										//System.out.println("2:" + loTargetConcept.getNameSpace());								    								
										Resource loResSubject = (Resource)loBlankNodeSubjectMap;
										Property loPropPredicate = loClass;
										RDFNode loResObject = ResourceFactory.createResource(lsNameSpace + lsClassNameTableValue); 
										aoMappingsModel.remove(loStateClass);							    								
										loResSubject.addProperty(loPropPredicate, loResObject);
									}
			    				}
			    				
			    				// Possible data properties to be DELETED -----------------------------------------------------
			    				llstToDelete = new ArrayList<Statement>();
			    				int liNumDataProps = 0;
			    				
			    				// count number of properties...
			    				StmtIterator loIterPredicateObjectMap = aoMappingsModel.listStatements(loClassMapping, loPredicateObjectMap, (RDFNode)null);
					    		while (loIterPredicateObjectMap.hasNext()){
					    			Statement loStatPredicateObjectMap = loIterPredicateObjectMap.next();
					    			RDFNode loBlankNodePredicateObjectMap = loStatPredicateObjectMap.getObject();
					    			if(loBlankNodePredicateObjectMap != null){
					    				StmtIterator loIterObjectMap = aoMappingsModel.listStatements((Resource)loBlankNodePredicateObjectMap, loObjectMap, (RDFNode)null);
					    				if (loIterObjectMap.hasNext()){
					    					Statement loStatObjectMap = loIterObjectMap.next();
					    					RDFNode loBlankNodeObjectMap = loStatObjectMap.getObject();
					    					if(loBlankNodeObjectMap != null){
					    						StmtIterator loIterParentTriplesMap = aoMappingsModel.listStatements((Resource)loBlankNodeObjectMap, loParentTriplesMap, (RDFNode)null);
					    						if(!loIterParentTriplesMap.hasNext()) {
					    							liNumDataProps++;
					    							/*
					    							// Test:
					    							StmtIterator loIterPredicate = aoMappingsModel.listStatements((Resource)loBlankNodePredicateObjectMap, loPredicate, (RDFNode)null);
								    				if (loIterPredicate.hasNext()){
								    					Resource loTargetConceptName = (Resource)loIterPredicate.next().getObject();
								    					System.out.println("data property?:" + loTargetConceptName.getLocalName());
								    				}
								    				*/
					    						}
					    					}
					    				}
				    				}
					    		}			    				
											
								for (int i=liRow; i < liRow + liNumDataProps; i++) {
									Statement loStatTargetConceptTriple = (Statement) moRDFData[i][1];
									Boolean loJCheckedDP = (Boolean) moMappingsTable.getModel().getValueAt(i, 3);
									if (loJCheckedDP) {
										// Possible data properties to be UPDATED -----------------------------------------
										String lsDataPropertyNameTableValue = (String) moMappingsTable.getModel().getValueAt(i, 5);
										loIterPredicateObjectMap = aoMappingsModel.listStatements(loClassMapping, loPredicateObjectMap, (RDFNode)null);
							    		if (loIterPredicateObjectMap.hasNext()){
							    			RDFNode loBlankNodePredicateObjectMap = loIterPredicateObjectMap.next().getObject();
							    			if(loBlankNodePredicateObjectMap != null){
							    				StmtIterator loIterTriplesMaps = aoMappingsModel.listStatements((Resource)loBlankNodePredicateObjectMap, loObjectMap, (RDFNode)null);
							    				StmtIterator loIterPredicate = aoMappingsModel.listStatements((Resource)loBlankNodePredicateObjectMap, loPredicate, (RDFNode)null);
							    				if (loIterPredicate.hasNext()){
							    					Resource loTargetConceptName = (Resource)loIterPredicate.next().getObject();			    					
								    				if (loIterTriplesMaps.hasNext()){
								    					RDFNode loBlankNodeObjectMap = loIterTriplesMaps.next().getObject();
								    					if(loBlankNodeObjectMap != null){
								    						StmtIterator loIterParentTriplesMap = aoMappingsModel.listStatements((Resource)loBlankNodeObjectMap, loParentTriplesMap, (RDFNode)null);
								    						if(!loIterParentTriplesMap.hasNext()) {
								    							// Is a data property:
								    							// Review COMBOBOX:								    							
								    							if(!lsDataPropertyNameTableValue.equals(loTargetConceptName.getLocalName())) {
																	if(!verifyLocalName(lsDataPropertyNameTableValue)) {
																		moTAOutput.append("Error: the name of the Data Property " + lsDataPropertyNameTableValue + " is not valid\n");
																		lsDataPropertyNameTableValue = loTargetConceptName.getLocalName(); 
																	}
																}
								    							String lsNameSpace = aoMappingsModel.getNsPrefixURI((String) moMappingsTable.getModel().getValueAt(i, 4));
								    							Resource loTargetConcept = (Resource)loStatTargetConceptTriple.getObject();								    							
								    							if(!lsNameSpace.equals(loTargetConcept.getNameSpace())){
								    								// Create a new resource:
								    								//System.out.println("1:" + lsNameSpace);
								    								//System.out.println("2:" + loTargetConcept.getNameSpace());								    								
								    								Resource loResSubject = loStatTargetConceptTriple.getSubject();
								    								Property loPropPredicate = loStatTargetConceptTriple.getPredicate();
								    								aoMappingsModel.remove(loStatTargetConceptTriple);								    								
								    								Resource loResObject = ResourceFactory.createResource(lsNameSpace + lsDataPropertyNameTableValue);
								    								loResSubject.addProperty(loPropPredicate, loResObject);
								    							}
								    						}
								    					}
								    				}
							    				}
							    			}
							    		}
									}
									else{
										// Data Properties to be deleted ------------------------------------------------------
										/* 
										 * Example:
										 * 
										 * rr:predicateObjectMap [
										 * 	 rr:predicate vocab:dbo_CAT_TaxonomySystems_deleted;
										 *   rr:objectMap [ rr:column '"deleted"'; ];
										 * ];
										 * 
										 * map:dbo_CAT_TaxonomySystems  rr:predicateObjectMap 	bj826324827642428
										 * bj826324827642428  			rr:predicate 			vocab:dbo_CAT_TaxonomySystems_deleted; <---- loStatTargetConceptTriple
										 */
										
										// IMPORTANT: there is not a delete cascade system for blank nodes in JENA! 
										StmtIterator loTriplesToDelete = aoMappingsModel.listStatements((Resource) null, null, loStatTargetConceptTriple.getSubject());
										if (loTriplesToDelete.hasNext()) {										
											llstToDelete.add(loTriplesToDelete.next());
											loTriplesToDelete = aoMappingsModel.listStatements(loStatTargetConceptTriple.getSubject(), loObjectMap, (RDFNode)null);
											if (loTriplesToDelete.hasNext()) {
												llstToDelete.add(loTriplesToDelete.next());
												aoMappingsModel.remove(llstToDelete);
											}
										}
									}							
								}
								aoMappingsModel.remove(llstToDelete);
								liRow+=liNumDataProps;
								// --------------------------------------------------------------------------------------------
								// Review and delete object properties:
								checkObjectProperties(aoMappingsModel);
								removeUncheckedProperties(aoMappingsModel);
							}
						}					
						else liRow++; 
					}
				}
			}
			else liRow++;
		}
		
		/*
		// TEST
		StmtIterator loIter = aoMappingsModel.listStatements();
		while (loIter.hasNext()){
			Statement loState = loIter.next();
			System.out.println(loState.getSubject() + " " + loState.getPredicate() + " " + loState.getObject());
		}
		*/
	}	
	
	// Method: Update the D2RQ mappings RDF model 
	public void updateD2RQMappingsFromTable(Model aoMappingsModel) {
		ArrayList<Statement> llstToDelete = new ArrayList<Statement>();
		Property loJoinProp = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "join");
		Property loBelongsToClassMap = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "belongsToClassMap");
		Property loD2rqClass = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "class");
    	Property loD2rqProperty = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "property");		
		for (int liRow = 0; liRow < moMappingsTable.getRowCount(); ) {
			// classes:
			String lsClassNameTableValue = (String) moMappingsTable.getModel().getValueAt(liRow, 2);
			if (!lsClassNameTableValue.equals("")) {
				// get the Resource:
				Resource loClassMapping = (Resource) moRDFData[liRow][0];
				if(!loClassMapping.equals(null)) {
					Boolean loJChecked = (Boolean) moMappingsTable.getModel().getValueAt(liRow, 0);
					if (!loJChecked) {
						// DELETE the CLASS and their data and object properties have to be deleted -------------------
						StmtIterator loIter1 = aoMappingsModel.listStatements(null, loBelongsToClassMap, loClassMapping);
						llstToDelete = new ArrayList<Statement>(); 
						while (loIter1.hasNext()) {
							Statement loStat = loIter1.next();
							Resource loDataPropertySubject = loStat.getSubject();
							StmtIterator loIter2 = aoMappingsModel.listStatements(loDataPropertySubject, null, (RDFNode)null);
							while (loIter2.hasNext()) {
								Statement loStat2 = loIter2.next();
								llstToDelete.add(loStat2);
							}
							liRow++;
						}
						// Delete the mapping of the class instance:
						StmtIterator loIter3 = aoMappingsModel.listStatements(loClassMapping, null, (RDFNode)null);
						while (loIter3.hasNext()) {
							Statement loStat = loIter3.next();
							llstToDelete.add(loStat);
						}
						aoMappingsModel.remove(llstToDelete);
					} else {
						// LOCAL NAME has changed? --------------------------------------------------------------------
						// Retrieve the name of the final class
						StmtIterator loIter = aoMappingsModel.listStatements(loClassMapping, loD2rqClass, (RDFNode)null);
						if (loIter.hasNext()) {
							// [ map:dbo_CAT_Categories d2rq:class <<vocab:dbo_CAT_Categories>> ]
							Resource loFinalClassIntanceName = (Resource)(loIter.next()).getObject();  
							if(!lsClassNameTableValue.equals(loFinalClassIntanceName.getLocalName())){
								// 1. Verify chars
								if(verifyLocalName(lsClassNameTableValue)) {
									// 2. Update the new name in the model
									String loNewURI = loFinalClassIntanceName.getNameSpace() + lsClassNameTableValue;
									ResourceUtils.renameResource(loFinalClassIntanceName, loNewURI);
								} else {
									moTAOutput.append("Error: the name of the class " + lsClassNameTableValue + " is not valid\n");
									lsClassNameTableValue = loFinalClassIntanceName.getLocalName(); // Restore the original name. <--------???
								}
							}
							// Possible data properties to be DELETED -----------------------------------------------------
							llstToDelete = new ArrayList<Statement>();
							int liNumDataProps = 0;
							StmtIterator loIter1 = aoMappingsModel.listStatements(null, loBelongsToClassMap, loClassMapping);
							while (loIter1.hasNext()) {
								// Discard data object properties for this table:
								StmtIterator loIterJoin = aoMappingsModel.listStatements(loIter1.next().getSubject(), loJoinProp, (RDFNode)null);
								if (!loIterJoin.hasNext()) {
									liNumDataProps++;
								}
							}						
							for (int i=liRow; i < liRow + liNumDataProps; i++) {
								Resource loDataPropertyMapping = (Resource) moRDFData[i][1];
								Boolean loJCheckedDP = (Boolean) moMappingsTable.getModel().getValueAt(i, 3);
								if (loJCheckedDP) {
									// Possible data properties to be UPDATED -----------------------------------------
									String lsDataPropertyNameTableValue = (String) moMappingsTable.getModel().getValueAt(i, 5);
									StmtIterator loIter3 = aoMappingsModel.listStatements(loDataPropertyMapping, loD2rqProperty, (RDFNode)null);
									if (loIter3.hasNext()) {
										Resource loFinalDataPropertyIntanceName = (Resource)(loIter3.next()).getObject();  
										if(!lsDataPropertyNameTableValue.equals(loFinalDataPropertyIntanceName.getLocalName())) {
											if(verifyLocalName(lsDataPropertyNameTableValue)) {
												String loNewURI = loFinalDataPropertyIntanceName.getNameSpace() + lsDataPropertyNameTableValue;
												ResourceUtils.renameResource(loFinalDataPropertyIntanceName, loNewURI);												
											} else {
												moTAOutput.append("Error: the name of the Data Property " + lsDataPropertyNameTableValue + " is not valid\n");
											}
										} else {
											// This property name has not been modified by the user.
										}
									}
									else {
										moTAOutput.append("Error: Object of the property name " + lsDataPropertyNameTableValue + " not found.\n");
									}
								}
								else{
									// Data Properties to be deleted ------------------------------------------------------
									StmtIterator loIter2 = aoMappingsModel.listStatements(loDataPropertyMapping, null, (RDFNode)null);
									while (loIter2.hasNext()) {
										Statement loStat2 = loIter2.next();
										llstToDelete.add(loStat2); // data property to be deleted:
									}
								}							
							}
							aoMappingsModel.remove(llstToDelete);
							liRow+=liNumDataProps;
							// --------------------------------------------------------------------------------------------
							// Review and delete object properties:
							checkObjectProperties(aoMappingsModel);
							removeUncheckedProperties(aoMappingsModel);
						}
						else liRow++; 
					}
				}
			}
			else liRow++;
		}
	}
	
	private void checkObjectProperties(Model aoMappingsModel) {
		ArrayList<Statement> llstToDelete = new ArrayList<Statement>();
		RDFNode loClassMap = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "ClassMap");
		Property loJoinProp = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "join");
		Property loBelongsToClassMap = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "belongsToClassMap");
    	Property loRefersToClassMap = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "refersToClassMap"); // range
		StmtIterator loOP = aoMappingsModel.listStatements((Resource)null, loJoinProp, (RDFNode)null);
		while (loOP.hasNext()) {
			// Iterate Object properties:
			Resource loObjPropMappingInstance = loOP.next().getSubject();
			StmtIterator loIterDomain = aoMappingsModel.listStatements(loObjPropMappingInstance, loBelongsToClassMap, (RDFNode)null);
			if (loIterDomain.hasNext()) {
				// Search the class
				StmtIterator loIterDomainConceptDefinition = aoMappingsModel.listStatements((Resource)(loIterDomain.next().getObject()), null, loClassMap);
				if(loIterDomainConceptDefinition.hasNext()) {
					// At least there is one class in the domain...
					//System.out.println("domain: "+ ((Resource)loIterDomain.next().getObject()).getLocalName());					
					StmtIterator loIterRange = aoMappingsModel.listStatements(loObjPropMappingInstance, loRefersToClassMap, (RDFNode)null);
					if (loIterRange.hasNext()) {
						RDFNode loIterRangeConcept = loIterRange.next().getObject();
						if(loIterRangeConcept.isResource()){
							// Resource:
							StmtIterator loIterRangeConceptDefinition = aoMappingsModel.listStatements((Resource)loIterRangeConcept, null, loClassMap);
							if (!loIterRangeConceptDefinition.hasNext()) {
								llstToDelete = deleteObjectProperty(aoMappingsModel, loObjPropMappingInstance, llstToDelete);
							}
						}else{
							// Literal: no need to check
						}
					}
				} else {
					// All classes in the domain have been removed, so the property has to be deleted.
					llstToDelete = deleteObjectProperty(aoMappingsModel, loObjPropMappingInstance, llstToDelete);
				}
			}
		}
		if(llstToDelete.size() > 0){
			aoMappingsModel.remove(llstToDelete);
		}		
	}
	
	// Method: delete the properties unchecked by the user and that have not
	// been previously removed due to lack of domain / range
	private void removeUncheckedProperties(Model aoMappingsModel) {
		ArrayList<Statement> llstToDelete = new ArrayList<Statement>();
		Property loD2rqProperty = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "property");
		for (int liRow = 0; liRow < moMappingsTableObjProps.getRowCount(); liRow++) {
			Boolean loJChecked = (Boolean) moMappingsTableObjProps.getModel().getValueAt(liRow, 0);
			if (!loJChecked) {
				Resource loObjProp = (Resource) moRDFDataObjProps[liRow][1];
				if(loObjProp != null){
					StmtIterator loIterObjProp = aoMappingsModel.listStatements(null, loD2rqProperty, (Resource) loObjProp);
					if (loIterObjProp.hasNext()) {
						StmtIterator loIter2 = aoMappingsModel.listStatements(loIterObjProp.next().getSubject(), null, (RDFNode)null);
						while (loIter2.hasNext()) {
							Statement loStat2 = loIter2.next();
							llstToDelete.add(loStat2); // data property to be deleted:
						}
					}
				} else {
					// The property has already been deleted.
				}
			} else {
				// Update the name of the object property:
				// 1. Check the original data 
				// 2. compare the original data with the current names in the table
				// 3. update the RDF model names.			
				// Retrieve the original name in the table (target names, not the name of the instance in the d2rq mapping)
				Resource loObjProp = (Resource) moRDFDataObjProps[liRow][1];
				String lsObjectPropTable = (String)moMappingsTableObjProps.getModel().getValueAt(liRow, 3);
				if (!loObjProp.getLocalName().equals(lsObjectPropTable)) {
					// UPDATE the OBJECT PROPERTY name:
					if(verifyLocalName(lsObjectPropTable)) {
						// 2. Update the new name in the model
						String loNewURI = loObjProp.getURI();
						loNewURI = moRDFMgr.excludeLocalNameFromURI(loNewURI) + lsObjectPropTable; 
						ResourceUtils.renameResource(loObjProp, loNewURI);
					} else {
						moTAOutput.append("Error: the name of the class " + lsObjectPropTable + " is not valid\n");
					}
				}
			}
		}
		if(llstToDelete.size() > 0){
			aoMappingsModel.remove(llstToDelete);
		}	
	}
	
	private ArrayList<Statement> deleteObjectProperty(Model aoMappingsModel, Resource aoObjProp, ArrayList<Statement> llstToDelete){
		Property loD2rqProperty = aoMappingsModel.getProperty(aoMappingsModel.getNsPrefixURI("d2rq"), "property");
		for (Object[] loRes : moRDFDataObjProps) {
			StmtIterator loIterObjProp = aoMappingsModel.listStatements(null, loD2rqProperty, (Resource) loRes[1]);
			if (loIterObjProp.hasNext()) {
				Resource loObjPropMappingInstance = loIterObjProp.next().getSubject();
				if (loObjPropMappingInstance.equals(aoObjProp)) {
					loRes[1] = null;
				}
			}
		}		
		StmtIterator loIter2 = aoMappingsModel.listStatements(aoObjProp, null, (RDFNode)null);
		while (loIter2.hasNext()) {
			Statement loStat2 = loIter2.next();
			llstToDelete.add(loStat2); // data property to be deleted:
		}
		return llstToDelete;
	}							
	
	private boolean verifyLocalName(String asLocalName) {
		if(asLocalName.contains(" ")||asLocalName.equals("")){
			return false;
		}
		// More checkings...
		return true;
	}
	
	public void removePattern(Model aoMappingsModel, String asPattern, boolean abUseR2RML){
		
		// IMPORTANT: these changes are not visible for the users, only for the algorithm that:
		// 1. Update the values to the model 
		// 2. Delete these tables
		// 3. Regenerate the tables from the model.
		
		// Table class and data properties
		// "Selected", "LName", "Classes", "Selected", "LName", "Data Properties"
		TableModel loTableModel = moMappingsTable.getModel();
		for(int liRow=0; liRow < loTableModel.getRowCount(); liRow++)	{
			String lsLocalName = moMappingsTable.getModel().getValueAt(liRow, 2).toString();
			if(lsLocalName.contains(asPattern)){
				lsLocalName = lsLocalName.replace(asPattern, "");
				loTableModel.setValueAt(lsLocalName, liRow, 2);
			}			
			lsLocalName = moMappingsTable.getModel().getValueAt(liRow, 5).toString();
			if(lsLocalName.contains(asPattern)){
				lsLocalName = lsLocalName.replace(asPattern, "");
				loTableModel.setValueAt(lsLocalName, liRow, 5);
			}
		}		
		moMappingsTable.setModel(loTableModel);
		
		// Table object properties:
		// "Selected", "Domain", "LName", "Object properties", "Range"
		loTableModel = moMappingsTableObjProps.getModel();
		for(int liRow=0; liRow < loTableModel.getRowCount(); liRow++)	{
			String lsLocalName = loTableModel.getValueAt(liRow, 3).toString();
			if(lsLocalName.contains(asPattern)){
				lsLocalName = lsLocalName.replace(asPattern, "");
				loTableModel.setValueAt(lsLocalName, liRow, 3);
			}			
		}		
		moMappingsTableObjProps.setModel(loTableModel);

		try {
			if (abUseR2RML) {
				updateR2RMLMappingsFromTable(aoMappingsModel);
			} else {
				updateD2RQMappingsFromTable(aoMappingsModel);
			}
		} catch (Exception e) {
			System.out.println(e);	
		}
	}
	
	// ----------------------------------------------------------------

	public void setOutPut(JTextArea aoTAOutput) {
		moTAOutput = aoTAOutput;
	}
	
	public int getNumRows(){
		return moMappingsTable.getRowCount();
	}

	public JTable getMappingsTable() {
		return moMappingsTable;
	}	
	
	public JTable getMappingsTableObjProps() {
		return moMappingsTableObjProps;
	}
	
	public JTable getMappingsTablePrefixes() {
		return moPrefixes;
	}
}
