package url.salle.arc.db2rdf.controls;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.font.TextAttribute;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;

public class ButtonControl {

	public static JButton createRegularButton(String asCaption, int aiWidth){
		return createRegularButton(asCaption, aiWidth, 20);
	}
	
	public static JButton createRegularButton(String asCaption, int aiWidth, int aiheight){
		
		JButton loJButton = new JButton(asCaption);
		loJButton.setBackground(new Color(200, 200, 200));
		loJButton.setMargin(new Insets(0, 0, 0, 0));
		loJButton.setPreferredSize(new Dimension(aiWidth,aiheight));
		loJButton.setMaximumSize(new Dimension(aiWidth,aiheight));
		return loJButton;
	}
	
	public static JButton createPropertyButton(String asCaption, Object aoProperty){
		
		JButton loJButton = new JButton(asCaption);
		loJButton.setBackground(new Color(255, 255, 255));
		loJButton.setForeground(Color.DARK_GRAY);
		loJButton.setFocusPainted(false);
		loJButton.setFont(new Font("Tahoma", Font.ITALIC, 12));//http://answers.yahoo.com/question/index?qid=20070906133202AAOvnIP
		loJButton.setOpaque(false);
		loJButton.setContentAreaFilled(false);
		loJButton.setBorderPainted(false);
		//loJButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
		return loJButton;		
	}
	
	public static JButton createObjectButton(String asCaption, Object aoObject){
		
		JButton loJButton = new JButton(asCaption);
		loJButton.setBackground(new Color(255, 255, 255));
		loJButton.setForeground(Color.BLUE);
		loJButton.setFocusPainted(false);
		loJButton.setFont(new Font("Tahoma", Font.PLAIN ,12));//http://answers.yahoo.com/question/index?qid=20070906133202AAOvnIP
		loJButton.setOpaque(false);
		loJButton.setContentAreaFilled(false);
		loJButton.setBorderPainted(false);
		loJButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
		
		Font font = loJButton.getFont();
		Map attributes = font.getAttributes();
		attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		loJButton.setFont(font.deriveFont(attributes));
		
		loJButton.setBorder(BorderFactory.createCompoundBorder(
		        BorderFactory.createLineBorder(Color.CYAN, 0), 
		        BorderFactory.createEmptyBorder(0, 25, 10, 5)));
		
		return loJButton;
	}
}
