package url.salle.arc.db2rdf.controls;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JTextArea;

public class TextAreaControl {
	public static JTextArea create(String asText, boolean abEditable, int aiHeight) {
		JTextArea loTA = new JTextArea();		
		loTA.setText(asText);
		Dimension d = loTA.getPreferredSize();
        d.height = aiHeight;
        loTA.setPreferredSize(d);
		loTA.setLineWrap(true);
		loTA.setWrapStyleWord(true);
		loTA.setEditable(abEditable);
		loTA.setBorder(	BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY), 
		            	BorderFactory.createEmptyBorder(3, 5, 2, 2)));
		return loTA;
	}
	
	public static JTextArea create(String asText, boolean abEditable, int aiHeight, int aiWidth) {
		JTextArea loTA = new JTextArea();		
		loTA.setText(asText);
        loTA.setPreferredSize( new Dimension(aiHeight, aiWidth));
		loTA.setLineWrap(true);
		loTA.setWrapStyleWord(true);
		loTA.setEditable(abEditable);
		loTA.setBorder(	BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY), 
		            	BorderFactory.createEmptyBorder(3, 5, 2, 2)));
		return loTA;
	}
}
