package url.salle.arc.db2rdf.controls;

import java.awt.Color;
import java.awt.Insets;

public class Global {

	 public static final Color BACKGROUND = Color.WHITE;
	 public static final Color FOREGROUND = Color.BLACK;
	 
	 public static final int BUTTON_HEIGHT = 22;
	 public static final Insets INSETS_PANEL_DEFAULT = new Insets(3, 5, 3, 2);
	 public static final Insets INSETS_PANEL_ZERO = new Insets(0, 0, 0, 0);
}



