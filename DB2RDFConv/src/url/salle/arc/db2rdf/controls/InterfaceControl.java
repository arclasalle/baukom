package url.salle.arc.db2rdf.controls;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.sparql.util.Utils;
import org.apache.jena.util.FileManager;

import url.salle.arc.db2rdf.Connection;
import url.salle.arc.db2rdf.ConnectionMgr;
import url.salle.arc.db2rdf.Mapping;
import url.salle.arc.db2rdf.RDFMgr;
import url.salle.arc.db2rdf.UtilsApp;
import url.salle.arc.db2rdf.controls.CustomDialogControl;

public class InterfaceControl extends JPanel implements TreeSelectionListener {
	
	// Members: variables
	private static boolean mbUseR2RML = true;
	private static String msMappingsFilePath = "";
	private static String msRDFDataFilePath = "";
	// Members: objects
	private static RDFMgr moRDFMgr;
	private static Mapping moMapping;
	// Members: controls
	private static JFrame moJFMain;
	private static JTextArea moTAOutput;
	private static JTextArea moTAOntologyUri;
	private static JSplitPane moSplitPaneVertical;	
	private static JTextField moTFDomain;  
	private static JTextField moTFInstance;
	private static JTextField moTFDataBase;
	private static JTextField moTFPort;
	private static JComboBox moCBConnectorType;
	private static JTextField moTFUser;
	private static JPasswordField moJPPassword;	
	private static JLabel moLADomain; 
	private static JLabel moLAInstance;
	private static JLabel moLADataBase;
	private static JLabel moLAPort;
	private static JLabel moLAConnectorType;
	private static JLabel moLAUser;
	private static JLabel moLAUserPassword;
	// Members: Mappings
	private static TableControl moTableControl;
	private static JButton moBTEditConnection;
	private static JButton moBTSaveConnection;
	private static JButton moBTGenMappingsFromDB;
	private static JButton moBTGenGraphFromDB;
	private static JButton moBTGenGraphFromMappings;
	private static JButton moBTSaveMappings;
	private static JButton moBTLoadMappings;
	private static JButton moBTUpdateMappings;
	private static JButton moBTRemovePattern;
	private static JButton moBTAddPrefix;
	private static JButton moBTAddPrefixFromLib;
	private static JTextArea moTATextPattern;
	// Members: Data transformation
	private static JButton moBTGenerateRDFData;	
	private static Container moCOWorkMappingsPanel;
	private static Container moCOWorkRDFdataPanel;
	private static JPanel moJPSecondBTsMappings;
	private static JPanel moJPSecondBTsTransfData;
	private static JScrollPane moJScrollPaneMappings;
	private static JScrollPane moJScrollPaneTransfData;
	private static GridBagConstraints moGBCMappingsPanel;
	private static GridBagConstraints moGBCTransfDataPanel;
	
	private Map<String, Model> moPrefixesList;
	
	// Constructor:
	public InterfaceControl(RDFMgr aoRDFMgr) {
		
		moRDFMgr = aoRDFMgr;
		moMapping = new Mapping(moRDFMgr);
		moTableControl = new TableControl(moRDFMgr);
		moPrefixesList = new HashMap<String, Model>();
		
		
		//UtilsApp.testConnectionToDataBase(); 
		
		
		
		JPanel loTopPanel = new JPanel();
		JPanel loBottomPanel = new JPanel();
		loBottomPanel.setLayout(new BorderLayout());
		loBottomPanel.add(createOutput(), BorderLayout.CENTER);
		
		Dimension minimumSize = new Dimension(10, 10);
		loTopPanel.setMinimumSize(minimumSize);
		loBottomPanel.setMinimumSize(minimumSize);

		loTopPanel.setLayout(new BorderLayout());
		loTopPanel.add(this.addTopPanel(), BorderLayout.NORTH);
		loTopPanel.add(this.addMainPanel(), BorderLayout.CENTER);
		loTopPanel.add(this.addDownPanel(), BorderLayout.SOUTH);

		Dimension wndSize = new Dimension(1000, 800);
		moSplitPaneVertical = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		moSplitPaneVertical.setTopComponent(loTopPanel);
		moSplitPaneVertical.setBottomComponent(loBottomPanel);
		moSplitPaneVertical.setResizeWeight(0.9);		
		moSplitPaneVertical.validate();
		moSplitPaneVertical.setPreferredSize(wndSize);

		this.add(moSplitPaneVertical);

		setListeners();

		ConnectionMgr.loadConnectionConfig();
		this.setConnectionFields();
		
		moTAOutput.append("done!\n");
		moMapping.setOutPut(moTAOutput);
		moTableControl.setOutPut(moTAOutput);
	}
	
	protected JPanel addTopPanel() {
		
		JPanel loJPTopContainer =  new JPanel();
		loJPTopContainer.setBorder(new EmptyBorder(4, 4, 4, 4));
		
		JLabel loLAOntologyUri = LabelControl.create("OntologyURILabel", "Ontology: ", 60);
		moTAOntologyUri = TextAreaControl.create("<none>", false,  Global.BUTTON_HEIGHT);
		
		Container loCITopContainer =  loJPTopContainer;
		GridBagLayout gbl = new GridBagLayout();
		loCITopContainer.setLayout(gbl);
		loCITopContainer.add(ComponentControl.addGrid(gbl, loLAOntologyUri, 1, 0, 0, 1, 1, 0.0D, 0.1D, null));
		loCITopContainer.add(ComponentControl.addGrid(gbl, moTAOntologyUri, 1, 1, 0, 1, 1, 0.2D, 0.1D, null));
		loCITopContainer.setPreferredSize(new Dimension(32767, 40));
		//loJPTopContainer.setBackground(Color.cyan);
		
		// Spinner:
		JPanel loPanelSpinnerloader = new JPanel();
		loCITopContainer.add(loPanelSpinnerloader);		
		moRDFMgr.setSpinner(loPanelSpinnerloader);
		
		return loJPTopContainer;
	}
	
	protected JPanel addMainPanel() {
		
		// Create TABs:
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Manage Mappings", createMappingsPanel());
        tabbedPane.addTab("Transform data", createTransfDataPanel());
        
        // ----------------------------------------------------------------------------
        
        JPanel loPLControlConnection = new JPanel();
        Dimension d = loPLControlConnection.getPreferredSize();
        d.width = 220;
        loPLControlConnection.setPreferredSize(d);
        loPLControlConnection.add(createConnectionPanel(), BorderLayout.CENTER);
        //loPanelDB.setBackground(Color.red);
        setControlsConnectionEditable(false);
        
        // ----------------------------------------------------------------------------

        JPanel panelButton = new JPanel(new GridLayout(1, 2, 0, 0));
        panelButton.setBorder(new EmptyBorder(0, 0, 0, 0)); 
        panelButton.add(tabbedPane);
        
        JPanel panelButton2 = new JPanel(new GridLayout(1, 1, 0, 0));
        panelButton2.add(loPLControlConnection);
        
        // Main panel:
        JPanel loMainPanel = new JPanel(new BorderLayout(4,4));
        loMainPanel.setBorder(new EmptyBorder(4,4,4,4));
        loMainPanel.add(panelButton, BorderLayout.CENTER);
        loMainPanel.add(panelButton2, BorderLayout.LINE_END);
        
        return loMainPanel;
	}  
    
	protected JPanel addDownPanel() {

		JPanel loDownPanel = new JPanel(new FlowLayout());
		loDownPanel.setBackground(Color.blue);
		return loDownPanel;
	}
	
	// --------------------------------------------------
	
   	protected Container createMappingsPanel() {		
   		
		moCOWorkMappingsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		moCOWorkMappingsPanel.setLayout(new GridBagLayout());
		
		moGBCMappingsPanel = new GridBagConstraints();
		
		moGBCMappingsPanel.fill = GridBagConstraints.BOTH;
		moGBCMappingsPanel.gridx = 0;
		moGBCMappingsPanel.gridy = 0;
		moGBCMappingsPanel.insets =  new Insets(1,6,0,0);		
		moCOWorkMappingsPanel.add(addPrimaryButtonsMappingsPanel(), moGBCMappingsPanel);
		
		moGBCMappingsPanel.weightx = 0;
		moGBCMappingsPanel.weighty = 0;
		moGBCMappingsPanel.gridx = 0;
		moGBCMappingsPanel.gridy = 1;
		moCOWorkMappingsPanel.add(addSecondaryButtonsMappingsPanel(), moGBCMappingsPanel);
	
		crateWorkMappingsPanel(null);
		
       	return moCOWorkMappingsPanel;
   	}
   	
   	protected Container createTransfDataPanel() {
   		
		moCOWorkRDFdataPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		moCOWorkRDFdataPanel.setLayout(new GridBagLayout());
		
		moGBCTransfDataPanel = new GridBagConstraints();
		
		moGBCTransfDataPanel.fill = GridBagConstraints.BOTH;
		moGBCTransfDataPanel.gridx = 0;
		moGBCTransfDataPanel.gridy = 0;
		moCOWorkRDFdataPanel.add(addPrimaryButtonsTransfDataPanel(), moGBCTransfDataPanel);
		
		moGBCTransfDataPanel.weightx = 0;
		moGBCTransfDataPanel.weighty = 0;
		moGBCTransfDataPanel.gridx = 0;
		moGBCTransfDataPanel.gridy = 1;
		moCOWorkRDFdataPanel.add(addSecondaryButtonsRDFDataPanel(), moGBCTransfDataPanel);
	
		crateWorkTransfDataPanel(null);
		
       	return moCOWorkRDFdataPanel;
   	}
   	
	protected JPanel addPrimaryButtonsMappingsPanel() {
		
		JPanel loJPPrimBTsMappings = new JPanel(new FlowLayout(FlowLayout.LEFT));
		loJPPrimBTsMappings.setBounds(0, 0, 0, 0);
		FlowLayout layout = (FlowLayout)loJPPrimBTsMappings.getLayout();
		layout.setHgap(0);		
		layout.setVgap(5);
		
		// Primary buttons:
		moBTGenMappingsFromDB = ButtonControl.createRegularButton("Generate mappings", 130, Global.BUTTON_HEIGHT);
		loJPPrimBTsMappings.add(moBTGenMappingsFromDB);
		moBTLoadMappings = ButtonControl.createRegularButton("Load", 45, Global.BUTTON_HEIGHT);
		loJPPrimBTsMappings.add(moBTLoadMappings);
		moBTSaveMappings = ButtonControl.createRegularButton("Save", 45, Global.BUTTON_HEIGHT);
		loJPPrimBTsMappings.add(moBTSaveMappings);
		
		loJPPrimBTsMappings.add(Box.createRigidArea(new Dimension(60,0)));
		
		JLabel loLAGraphFromDB = new JLabel("Generate graph:");
		loJPPrimBTsMappings.add(loLAGraphFromDB);
		loJPPrimBTsMappings.add(Box.createRigidArea(new Dimension(5,0)));
		moBTGenGraphFromDB = ButtonControl.createRegularButton("From DB", 65, Global.BUTTON_HEIGHT);
		loJPPrimBTsMappings.add(moBTGenGraphFromDB);
		moBTGenGraphFromMappings = ButtonControl.createRegularButton("From mappings", 100, Global.BUTTON_HEIGHT);
		loJPPrimBTsMappings.add(moBTGenGraphFromMappings);
		
		// spinner:
		JPanel loPanelSpinnerloaderGenMappings = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
   		layout = (FlowLayout)loPanelSpinnerloaderGenMappings.getLayout();
   		layout.setVgap(0);
   		loJPPrimBTsMappings.add(loPanelSpinnerloaderGenMappings);
   		moMapping.setSpinnerGenMapping(loPanelSpinnerloaderGenMappings);
		
		return loJPPrimBTsMappings;
	}
   	
	protected JPanel addSecondaryButtonsMappingsPanel() {
		
		moJPSecondBTsMappings = new JPanel(new FlowLayout(FlowLayout.LEFT));
		moJPSecondBTsMappings.setBorder(BorderFactory.createEmptyBorder());
       	moJPSecondBTsMappings.setBounds(0, 0, 0, 0);
		FlowLayout layout = (FlowLayout)moJPSecondBTsMappings.getLayout();
		layout.setHgap(0);
		layout.setVgap(0);		
       	
		moBTUpdateMappings = ButtonControl.createRegularButton("Update", 60, Global.BUTTON_HEIGHT);
		moJPSecondBTsMappings.add(moBTUpdateMappings);	
		
		moJPSecondBTsMappings.add(Box.createRigidArea(new Dimension(5,0)));
		
		moBTRemovePattern = ButtonControl.createRegularButton("Remove pattern", 110, Global.BUTTON_HEIGHT);
		moJPSecondBTsMappings.add(moBTRemovePattern);
		
		moTATextPattern = TextAreaControl.create("<none>", true, Global.BUTTON_HEIGHT);
		moTATextPattern.getDocument().putProperty("filterNewlines", Boolean.TRUE);		
		moTATextPattern.setPreferredSize(new Dimension(100, Global.BUTTON_HEIGHT));
		moJPSecondBTsMappings.add(moTATextPattern);
		
		moJPSecondBTsMappings.add(Box.createRigidArea(new Dimension(45,0)));
		
		JLabel loLAPrefix = new JLabel("Prefixes:");
		moJPSecondBTsMappings.add(loLAPrefix);
		
		moJPSecondBTsMappings.add(Box.createRigidArea(new Dimension(5,0)));
		
		moBTAddPrefix = ButtonControl.createRegularButton("Add", 40, Global.BUTTON_HEIGHT);
		moJPSecondBTsMappings.add(moBTAddPrefix);
		moBTAddPrefixFromLib = ButtonControl.createRegularButton("Add from LIB", 90, Global.BUTTON_HEIGHT);
		moJPSecondBTsMappings.add(moBTAddPrefixFromLib);
		
		return moJPSecondBTsMappings;
   	}   	
   	
   	protected JPanel addPrimaryButtonsTransfDataPanel() {

   		Insets loInsets = new Insets(3, 0, 3, 0);
   		
   		JPanel loJPPrimBTsTransfData = new JPanel(new GridLayout(2,1));
   		loJPPrimBTsTransfData.setBorder(new EmptyBorder(3,6,5,4));
   		
   		JPanel loTopSubPanelRow1 = new JPanel(new GridBagLayout());
   		JPanel loTopSubPanelRow2 = new JPanel(new GridBagLayout());

   		GridBagLayout moGBL = new GridBagLayout();   		
   		loTopSubPanelRow1.setLayout(moGBL);
   		loTopSubPanelRow2.setLayout(moGBL);
   		
   		JButton loBTLoadMappings = ButtonControl.createRegularButton("Mappings path", 130, Global.BUTTON_HEIGHT);
   		JTextArea loTAMappingFilePath = TextAreaControl.create("<none>", false, Global.BUTTON_HEIGHT);
   		moBTGenerateRDFData = ButtonControl.createRegularButton("Generate RDF data", 130, Global.BUTTON_HEIGHT);
   		JTextArea loTARDFDataFilePath = TextAreaControl.create("<none>", false, Global.BUTTON_HEIGHT);
  		JPanel loPanelSpinnerloaderRDFData = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
   		FlowLayout layout = (FlowLayout)loPanelSpinnerloaderRDFData.getLayout();
   		layout.setVgap(0);

   		loTopSubPanelRow1.add(loBTLoadMappings,    ComponentControl.addGrid(moGBL, GridBagConstraints.HORIZONTAL, 0.0, loInsets));
   		loTopSubPanelRow1.add(loTAMappingFilePath, ComponentControl.addGrid(moGBL, GridBagConstraints.HORIZONTAL, 1.0, loInsets));
   		loTopSubPanelRow2.add(moBTGenerateRDFData, ComponentControl.addGrid(moGBL, GridBagConstraints.HORIZONTAL, 0.0, loInsets));
   		loTopSubPanelRow2.add(loTARDFDataFilePath, ComponentControl.addGrid(moGBL, GridBagConstraints.HORIZONTAL, 1.0, loInsets));
   		loTopSubPanelRow2.add(loPanelSpinnerloaderRDFData, ComponentControl.addGrid(moGBL, GridBagConstraints.HORIZONTAL, 0.0, loInsets));
   		
   		moMapping.setSpinnerGenRDFData(loPanelSpinnerloaderRDFData);
       	
       	loJPPrimBTsTransfData.add(loTopSubPanelRow1);
       	loJPPrimBTsTransfData.add(loTopSubPanelRow2);
       	
    	// get the default directory where mappings are stored: 
		try {
			if(msMappingsFilePath.equals("")){
				msMappingsFilePath = new File(".").getCanonicalPath() + "\\output\\mapping_save.ttl";
			}
			loTAMappingFilePath.setText(msMappingsFilePath);
		} catch (IOException e) {
			e.printStackTrace();
		}

    	// get the default directory where RDF data generated will be stored: 
		try {
			if( msRDFDataFilePath.equals("")){
				msRDFDataFilePath = new File(".").getCanonicalPath() + "\\output\\data2.ttl";
			}
			loTARDFDataFilePath.setText(msRDFDataFilePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
       	return loJPPrimBTsTransfData;
   	}
   	
   	protected JPanel addSecondaryButtonsRDFDataPanel() {
   		moJPSecondBTsTransfData = new JPanel(new FlowLayout(FlowLayout.LEFT));
   		return moJPSecondBTsTransfData;
   	}
   	
   	protected JScrollPane createOutput(){
   	  	moTAOutput = new JTextArea();
   	  	moTAOutput.setText("initiating...\n");
       	JScrollPane loScroll = new JScrollPane(moTAOutput); //place the JTextArea in a scroll pane
       	return loScroll;
   	}
   	
   	protected JTextField createTextField(int aiWidth){   	
    	JTextField loJTextField = new JTextField();
    	loJTextField.setMargin(new Insets(0, 0, 0, 0));    	
    	loJTextField.setAlignmentX(Component.LEFT_ALIGNMENT);
    	return loJTextField;
    }
    
    protected JLabel createLabel(int aiWidth, String asCaption){
		JLabel loJLabel = new JLabel(asCaption);
		loJLabel.setPreferredSize(new Dimension(aiWidth,20));
    	loJLabel.setMaximumSize(new Dimension(aiWidth,20));  
    	loJLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
    	return loJLabel;
    }    
    
    protected JPanel createConnectionPanel(){
        	
    	int liWidth = 180;
    	    	
    	moLADomain = createLabel(liWidth, "Domain (e.g. 'localhost')"); 
    	moLAInstance = createLabel(liWidth, "Instance (e.g. 'INSTANCE\\SQLEXPRESS')");
    	moLADataBase =createLabel(liWidth, "Database (e.g., 'Employees')");
    	moLAPort = createLabel(liWidth, "Port (e.g. '1433')");
    	moLAConnectorType = createLabel(liWidth, "Driver (e.g. 'jdbc:sqlserver')");
    	moLAUser = createLabel(liWidth, "User");
    	moLAUserPassword = createLabel(liWidth, "Password");
    	
    	moTFDomain = createTextField(liWidth);
    	moTFInstance = createTextField(liWidth);
    	moTFDataBase = createTextField(liWidth);
    	moTFPort = createTextField(liWidth);
    	
    	String[] llstDriversStrings = { "jdbc:sqlserver" };
    	moCBConnectorType = new JComboBox(llstDriversStrings);
    	moCBConnectorType.setSelectedIndex(0);
    	moCBConnectorType.setAlignmentX(Component.LEFT_ALIGNMENT);
    	
    	moTFUser = createTextField(liWidth);
       	
    	moJPPassword = new JPasswordField(0);
    	moJPPassword.setMargin(new Insets(0, 0, 0, 0));
    	moJPPassword.setAlignmentX(Component.LEFT_ALIGNMENT);  	
    	
    	moBTEditConnection = ButtonControl.createRegularButton("Edit", 50);
    	moBTSaveConnection = ButtonControl.createRegularButton("Save", 50);
    			
    	JPanel loPanelButtons = new JPanel(new GridLayout(1,2));
    	loPanelButtons.setPreferredSize(new Dimension(100,Global.BUTTON_HEIGHT));
    	loPanelButtons.setMaximumSize(new Dimension(100,Global.BUTTON_HEIGHT));  
    	loPanelButtons.setAlignmentX(Component.LEFT_ALIGNMENT);    	
    	loPanelButtons.add(moBTEditConnection);
    	loPanelButtons.add(moBTSaveConnection);
    	
    	JPanel loPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    	loPanel.setLayout(new BoxLayout(loPanel, BoxLayout.Y_AXIS));    	
    	
    	JPanel loJPPictureDB = ImageControl.createIcon("img/database.png", new FlowLayout(FlowLayout.LEFT)); 
    	
    	loPanel.add(loJPPictureDB);
    	loPanel.add(Box.createVerticalStrut(2));
    	loPanel.add(moLADomain);
    	loPanel.add(Box.createVerticalStrut(2));
    	loPanel.add(moTFDomain); 	
    	loPanel.add(Box.createVerticalStrut(10));
    	loPanel.add(moLAInstance);
    	loPanel.add(Box.createVerticalStrut(2));
    	loPanel.add(moTFInstance);
    	loPanel.add(Box.createVerticalStrut(10));
		loPanel.add(moLADataBase);
		loPanel.add(Box.createVerticalStrut(2));
		loPanel.add(moTFDataBase);
		loPanel.add(Box.createVerticalStrut(10));
		loPanel.add(moLAPort);
		loPanel.add(Box.createVerticalStrut(2));
		loPanel.add(moTFPort);
		loPanel.add(Box.createVerticalStrut(10));
		loPanel.add(moLAConnectorType);
		loPanel.add(Box.createVerticalStrut(2));
		loPanel.add(moCBConnectorType);
		loPanel.add(Box.createVerticalStrut(10));
		loPanel.add(moLAUser);
		loPanel.add(Box.createVerticalStrut(2));
		loPanel.add(moTFUser);
		loPanel.add(Box.createVerticalStrut(10));
		loPanel.add(moLAUserPassword);
		loPanel.add(Box.createVerticalStrut(2));
		loPanel.add(moJPPassword);
		loPanel.add(Box.createVerticalStrut(10));
		loPanel.add(loPanelButtons);		
    	
		return loPanel;
    }
    
    protected void setListeners(){
    	
    	moBTEditConnection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setControlsConnectionEditable(!isEditable());
			}
		});
    	
    	moBTSaveConnection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveConnection();
			}
		});
    	
    	// Mappings
    	
    	moBTGenMappingsFromDB.addActionListener(new ActionListener() {
   			public void actionPerformed(ActionEvent e) {   				
   				updateConnector();   				
   				// The extraction of mappings process is generated in a independent thread:
   				moMapping.extractMappingsConcepts(mbUseR2RML);
   			}
   		});    	
    	
    	moBTLoadMappings.addActionListener(new ActionListener() {
   			public void actionPerformed(ActionEvent e) {
   				loadMappings();
   			}
   		});
    	
    	moBTSaveMappings.addActionListener(new ActionListener() {
   			public void actionPerformed(ActionEvent e) {
   				saveMappings();
   			}
   		});
    	
    	moBTGenGraphFromDB.addActionListener(new ActionListener() {
   			public void actionPerformed(ActionEvent e) {
   				generateGraph(true);
   			}
   		});   	
	
		moBTGenGraphFromMappings.addActionListener(new ActionListener() {
   			public void actionPerformed(ActionEvent e) {
   				generateGraph(false);
   			}
   		});   	
    	
    	moBTUpdateMappings.addActionListener(new ActionListener() {
   			public void actionPerformed(ActionEvent e) {
   				if(mbUseR2RML) {
   					moTableControl.updateR2RMLMappingsFromTable(moRDFMgr.getMappingsModel());
   				} else {
   					moTableControl.updateD2RQMappingsFromTable(moRDFMgr.getMappingsModel());
   				}   				
   				crateWorkMappingsPanel(moRDFMgr.getMappingsModel());
   			}
   		});    	
    	
    	moBTRemovePattern.addActionListener(new ActionListener() {
   			public void actionPerformed(ActionEvent e) {
   				removePattern();
   			}
   		});   	
    	
    	moBTAddPrefix.addActionListener(new ActionListener() {
   			public void actionPerformed(ActionEvent e) {
   				addPrefix();
   			}
   		});   	
    	
    	moBTAddPrefixFromLib.addActionListener(new ActionListener() {
   			public void actionPerformed(ActionEvent e) {
   				addPrefixFromLib();
   			}
   		});
    	
    	// Data transformation
    	
   		moBTGenerateRDFData.addActionListener(new ActionListener() {
   			public void actionPerformed(ActionEvent e) {
   				updateConnector();
   				// The data transformation process is generated in a independent thread:
   				
   				
   				//msMappingsFilePath = "d2rq-dev\\mappings.ttl"; 
   				//msRDFDataFilePath = "d2rq-dev\\data2.ttl";
   				
   				moMapping.generateRDFData(msMappingsFilePath, msRDFDataFilePath, mbUseR2RML);
   			}
   		});   		
    }
    
    // called from the mapping generator:
	public void loadMappingData() {
		Model loMappingModel = moRDFMgr.loadModel("output/mappings.ttl");
		if (loMappingModel != null) {
			moRDFMgr.setMappingsModel(loMappingModel);
			crateWorkMappingsPanel(loMappingModel);
		}
	}
    
    protected void saveConnection(){
		if(this.checkConfigFields()){			
			updateConnector();				
			ConnectionMgr.saveConnectionConfig();
		} else {
			System.out.println("Can't save the connection: one or more fields are empty.");
		}
    }
    
	// Method: required by TreeSelectionListener interface.
    public void valueChanged(TreeSelectionEvent e) {
    }    
	
    private void crateWorkMappingsPanel(Model aoMappingModel){

    	moTableControl.setEmptyTable();
    	if (aoMappingModel != null) {
    		if(mbUseR2RML){
    			moTableControl.createMappingTableR2RML(aoMappingModel);
    		} else {
    			moTableControl.createMappingTableD2RQ(aoMappingModel);
    		}
		}
		
    	JPanel loJPMappingsTables = new JPanel(new GridLayout(3,1)); 
    	loJPMappingsTables.add(new JScrollPane(moTableControl.getMappingsTable()));
    	loJPMappingsTables.add(new JScrollPane(moTableControl.getMappingsTableObjProps()));
    	loJPMappingsTables.add(new JScrollPane(moTableControl.getMappingsTablePrefixes()));
    	
    	moJScrollPaneMappings = new JScrollPane(loJPMappingsTables);
    	
    	int liNumComps = moCOWorkMappingsPanel.getComponents().length;
    	if(liNumComps > 2){
    		moCOWorkMappingsPanel.remove(2);    		
    	}
    		
    	moGBCMappingsPanel.weightx = 1;
    	moGBCMappingsPanel.weighty = 1;
    	moGBCMappingsPanel.gridx = 0;
    	moGBCMappingsPanel.gridy = 2;
		moCOWorkMappingsPanel.add(moJScrollPaneMappings, moGBCMappingsPanel);
		moCOWorkMappingsPanel.revalidate();
		moCOWorkMappingsPanel.repaint();		
		
		if (aoMappingModel == null) {
			moCOWorkMappingsPanel.setVisible(false);
		} else {
			moCOWorkMappingsPanel.setVisible(true);
		}
    }
    
    private void crateWorkTransfDataPanel(Model aoMappingModel){
    	
    	moJScrollPaneTransfData = new JScrollPane();
    	
    	int liNumComps = moCOWorkRDFdataPanel.getComponents().length;
    	if(liNumComps > 2){
    		moCOWorkRDFdataPanel.remove(2);
    	}
    	
    	moGBCTransfDataPanel.weightx = 1;
    	moGBCTransfDataPanel.weighty = 1;
    	moGBCTransfDataPanel.gridx = 0;
    	moGBCTransfDataPanel.gridy = 2;
    	moCOWorkRDFdataPanel.add(moJScrollPaneTransfData, moGBCTransfDataPanel);
    	moCOWorkRDFdataPanel.revalidate();
    	moCOWorkRDFdataPanel.repaint();
    }
 
    private void replaceComponent(Container container, JPanel aoNewPanel, int aiPos) {
    	int i=0;
    	Component loCO = null;
        for (Component c : container.getComponents()) {
        	if(i== aiPos){
        		c = aoNewPanel;
        	}
        }
    }
    
	private void disablePanel(JPanel loMainButtonsPanel, boolean enable) {
		for (Component c : loMainButtonsPanel.getComponents())
			c.setEnabled(enable);
	}    
    
	private void removePattern() {
		if (moRDFMgr.isLoaded()) {
			int lsResult = JOptionPane.showConfirmDialog(moJFMain, "Not updated changes will be lost. Do you want to proceed?", "Remove pattern", JOptionPane.YES_NO_OPTION);
			if(lsResult == JOptionPane.YES_OPTION) {
				moTableControl.removePattern(moRDFMgr.getMappingsModel(), moTATextPattern.getText(), mbUseR2RML);
				crateWorkMappingsPanel(moRDFMgr.getMappingsModel());
			}			
		}
	}
	
	// -----------------------------------------------------------

	public void loadMappings() {
		/*
		JFileChooser jFileChooser = new JFileChooser();
		jFileChooser.setSelectedFile(new File("mappings.ttl"));
		if (jFileChooser.showOpenDialog(moJFMain) == JFileChooser.APPROVE_OPTION) {
			File file = jFileChooser.getSelectedFile();
			moRDFMgr.loadModel(file.getPath());
			loadMappingData();
		}*/
		
		// Testing!
		//Model loMappingModel = moRDFMgr.loadModel("output\\mapping_save.ttl");
		Model loMappingModel = moRDFMgr.loadModel("C:\\Users\\Gons\\Desktop\\mappings.ttl");
		if (loMappingModel != null) {
			moRDFMgr.setMappingsModel(loMappingModel);
			crateWorkMappingsPanel(loMappingModel);
		}
	}	
	
	public void saveMappings() {
		JFileChooser jFileChooser = new JFileChooser();
		jFileChooser.setSelectedFile(new File("mappings.ttl"));
		if (jFileChooser.showSaveDialog(moJFMain) == JFileChooser.APPROVE_OPTION) {
			try {
				if (mbUseR2RML) {
					moTableControl.updateR2RMLMappingsFromTable(moRDFMgr.getMappingsModel());
				} else {
					moTableControl.updateD2RQMappingsFromTable(moRDFMgr.getMappingsModel());
				}

				File file = jFileChooser.getSelectedFile();
				moRDFMgr.saveModel(moTAOutput, moRDFMgr.getMappingsModel(), file.getPath());
			} catch (Exception e) {
			}
		}
		// moRDFMgr.saveModel(moTAOutput, moRDFMgr.getMappingsModel(),
		// "output\\mapping_save.ttl");
	}
	
	public void generateGraph(boolean abFromDB) {
		JFileChooser jFileChooser = new JFileChooser();
		jFileChooser.setSelectedFile(new File("graph.ttl"));
		if (jFileChooser.showSaveDialog(moJFMain) == JFileChooser.APPROVE_OPTION) {
			File file = jFileChooser.getSelectedFile();
			boolean lbResult = false;
			if(abFromDB){
				lbResult = moMapping.generateGraphFromDB(file.getPath(), mbUseR2RML);
			} else {
				lbResult = moMapping.generateGraphFromMappings(file.getPath(), moRDFMgr.getMappingsModel());
			} 
			if (lbResult) {
				JOptionPane.showMessageDialog(moJFMain, "Done.");
			} else {
				JOptionPane.showMessageDialog(moJFMain, "Error during the extraction of the graph.");
			}
		}
	}
	
	private void addPrefix() {
		if (moRDFMgr.isLoaded()) {
			int lsResult = JOptionPane.showConfirmDialog(moJFMain, "Not updated changes will be lost. Do you want to proceed?", "Add prefix", JOptionPane.YES_NO_OPTION);
			if(lsResult == JOptionPane.YES_OPTION) {
				String lsUrlPrefix = (String) JOptionPane.showInputDialog(moJFMain, "Add the URL of the new space name", "",
						JOptionPane.QUESTION_MESSAGE);
				if (lsUrlPrefix != null) {
					if (!lsUrlPrefix.equals("")) {
						String lsLocalNamePrefix = (String) JOptionPane.showInputDialog(moJFMain, "Add the local name", "",
								JOptionPane.QUESTION_MESSAGE);
						if (lsLocalNamePrefix != null) {
							if (!lsLocalNamePrefix.equals("")) {
								if ((!lsUrlPrefix.equals("")) && (!lsLocalNamePrefix.equals(""))) {
									if (moRDFMgr.addPrefix(lsLocalNamePrefix, lsUrlPrefix)) {
										// Add to the prefix table:
										crateWorkMappingsPanel(moRDFMgr.getMappingsModel());										
									} else {
										// default title and icon
										JOptionPane.showMessageDialog(moJFMain, "The name space already exists.");
									}
								} else {
									// default title and icon
									JOptionPane.showMessageDialog(moJFMain, "Invalid prefix.");
								}
							}
						} // Cancelled
					}
				} // Cancelled
			} // Cancelled
		} else {
			// default title and icon
			JOptionPane.showMessageDialog(moJFMain, "There are no mappings loaded");
		}
	}
	
	private void addPrefixFromLib() {
		if (moRDFMgr.isLoaded()) {
			// update changes:
			try {
				if(mbUseR2RML) {
					moTableControl.updateR2RMLMappingsFromTable(moRDFMgr.getMappingsModel());
				} else {
					moTableControl.updateD2RQMappingsFromTable(moRDFMgr.getMappingsModel());
				}
			
				String lsDirectory = "graphs";
				String[] laUpperOntologies = UtilsApp.getUpperOntologiesFileNames(lsDirectory);
				String lsFileName = (String) JOptionPane.showInputDialog(moJFMain, 
				        "Select the upper ontology to be added",
				        "upper ontology",
				        JOptionPane.QUESTION_MESSAGE, 
				        null, 
				        laUpperOntologies, 
				        laUpperOntologies[0]);
				if (lsFileName != null) {
					// get the URI and the prefix name:
					Model loUOModel = moRDFMgr.loadModel(lsDirectory + "//" + lsFileName);
					String lsPrefixName = moRDFMgr.getPrefixName(loUOModel);
					String lsPrefixURI = moRDFMgr.getPrefixURI(loUOModel);
					// check if the prefix is already defined
					if(!moRDFMgr.isExistPrefixName(lsPrefixName)) {
						if(!moRDFMgr.isExistPrefixUri(lsPrefixURI)) {
							if(moRDFMgr.addPrefix(lsPrefixName, lsPrefixURI)){
								JOptionPane.showMessageDialog(moJFMain, "Prefix added correctly.");
								crateWorkMappingsPanel(moRDFMgr.getMappingsModel());
								moPrefixesList.put(lsFileName, loUOModel);
							}
						} else{
							JOptionPane.showMessageDialog(moJFMain, "The name space already exists.");
						}
					} else {
						JOptionPane.showMessageDialog(moJFMain, "The name space already exists.");
					}
				}
			}
			catch(Exception e){
				JOptionPane.showMessageDialog(moJFMain, "Error: "+ e.toString());
			} 
		} else {
			// default title and icon
			JOptionPane.showMessageDialog(moJFMain, "There are no mappings loaded");
		}
	}
    	
	public Container getMainContainerObject() {
		return (Container) moSplitPaneVertical;
	}
	
    public boolean isEditable(){
    	return moTFDomain.isEditable();
    }
    
    public boolean checkConfigFields()
    {
    	if( !moTFDomain.getText().equals("") &&
			!moTFInstance.getText().equals("") &&
			!moTFDataBase.getText().equals("") &&
			!moTFPort.getText().equals("") &&
			(moCBConnectorType.getSelectedIndex() > -1) &&
			!moTFUser.getText().equals("") &&
			!moJPPassword.getPassword().equals("")){
    		return true;
    	}
    	return false;
    }   
    
    public void updateConnector()
    {
    	Connection.Domain = moTFDomain.getText();
    	Connection.Instance = moTFInstance.getText();
    	Connection.DefaultDBName = moTFDataBase.getText();
    	Connection.Port = moTFPort.getText();
		Connection.Driver = (String) moCBConnectorType.getItemAt(moCBConnectorType.getSelectedIndex());
		Connection.UserName	= moTFUser.getText();
		Connection.UserPasword = new String(moJPPassword.getPassword());
    }    
    
    public void setFrame(JFrame aoJFrame) {
    	moJFMain = aoJFrame;
    	moMapping.setFrame(aoJFrame);
    	moMapping.setInterface(this);    	
    }
    
    public void setControlsConnectionEditable(boolean lbEditable)
    {
		moTFDomain.setEditable(lbEditable);
    	moTFInstance.setEditable(lbEditable);
    	moTFDataBase.setEditable(lbEditable);
    	moTFPort.setEditable(lbEditable);
    	moCBConnectorType.setEnabled(lbEditable);
    	moTFUser.setEditable(lbEditable);		
    	moJPPassword.setEditable(lbEditable);
    }  
    
    public void setConnectionFields()
    {
		if (Connection.Domain != null) {
			moTFDomain.setText(Connection.Domain);
			moTFInstance.setText(Connection.Instance);
			moTFDataBase.setText(Connection.DefaultDBName);
			moTFPort.setText(Connection.Port);
			moCBConnectorType.setSelectedItem(Connection.Driver);
			moTFUser.setText(Connection.UserName);
			moJPPassword.setText(Connection.UserPasword);			
		} else {
			System.out.println("There is no information about the connection");
		}		
    }
    
    public void setOntologyURI(String asOntologyURI){
    	moTAOntologyUri.setText(asOntologyURI);
    }
    
    public JButton getEditButton() {
    	return moBTEditConnection;
    }
    
    public JButton getSaveButton() {
    	return moBTSaveConnection;
    }    
    
    public JButton getStartMappingsOntologyButton() {
    	return moBTGenMappingsFromDB;
    }
    
    public JTextArea getOutput() {
    	return moTAOutput;
    }
    
    public JButton  getGenerateRDFdata() {
    	return moBTGenerateRDFData;
    }
    
    public JPanel getWorkPanel() {
    	return (JPanel) moCOWorkMappingsPanel;
    }	
}





