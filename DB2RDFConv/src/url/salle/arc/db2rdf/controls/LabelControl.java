package url.salle.arc.db2rdf.controls;

import java.awt.Dimension;

import javax.swing.JLabel;

public class LabelControl {

	public static JLabel create(String name, String text, int prefWidth) {
		JLabel tempLabel = new JLabel(text, 4);
		tempLabel.setName(name);
		tempLabel.setPreferredSize(new Dimension(prefWidth, 10));
		return tempLabel;
	}
}
