package url.salle.arc.db2rdf.controls;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;

import url.salle.arc.db2rdf.RDFMgr;

public class MenuBarControl {

	private JTextArea output;
	private JScrollPane scrollPane;
	private JMenuBar moMenuBar;

	public MenuBarControl() 
	{ 
		moMenuBar = new JMenuBar();
	}
	
	public JMenuBar get()
	{
		return moMenuBar;
	}
	
	public JMenuBar create(RDFMgr aoRDFMgr, JFrame aoFrame, InterfaceControl aoMainPanel) {

		JMenu menu;
		JMenuItem menuItem;

		// Build the first menu.
		menu = new JMenu("File");
		menu.setMnemonic(KeyEvent.VK_A);
		menu.getAccessibleContext().setAccessibleDescription("The only menu in this program that has menu items");	
		moMenuBar.add(menu);

		// a group of JMenuItems
		menuItem = new JMenuItem("Load RDF file", KeyEvent.VK_T);		
		menuItem.setEnabled(false);		
		menuItem.getAccessibleContext().setAccessibleDescription("This doesn't really do anything");
		menuItem.addActionListener((ActionEvent event) -> {
			// not implemented			
		});
		menu.add(menuItem);
		
		// a group of JMenuItems
		menuItem = new JMenuItem("Load Ontology file", KeyEvent.VK_T);		
		menuItem.getAccessibleContext().setAccessibleDescription("This doesn't really do anything");
		menuItem.addActionListener((ActionEvent event) -> {
			
			//String lsFileName = load();
			String lsFileName = "C:\\Users\\Gons\\Desktop\\IFC4_ADD1.owl";
			//String lsFileName = "C:\\Users\\gcosta\\Desktop\\IFC4_ADD1.owl";
	                              
			// This process includes specific threads so the result can not be evaluated here:
			aoRDFMgr.loadOntology(lsFileName, aoFrame, aoMainPanel);			
		});
		menu.add(menuItem);
		
		return moMenuBar;
	}
	
	private String load()
    {
		JFrame f = new JFrame("Load");
		JFileChooser chooser = new JFileChooser();
        int returnVal = chooser.showOpenDialog(f);
        if(returnVal == JFileChooser.APPROVE_OPTION) 
        {
			File file = chooser.getSelectedFile();
			if (file != null) {
				return file.getPath();
			} else {
				JOptionPane.showMessageDialog(f, "Please enter a fileName", "Error", JOptionPane.ERROR_MESSAGE);
			}
        }
        return "";
    }
		
	public Container createContentPane() {
		
		// Create the content-pane-to-be.
		JPanel contentPane = new JPanel(new BorderLayout());
		contentPane.setOpaque(true);

		// Create a scrolled text area.
		output = new JTextArea(5, 30);
		output.setEditable(false);
		scrollPane = new JScrollPane(output);

		// Add the text area to the content pane.
		contentPane.add(scrollPane, BorderLayout.CENTER);

		return contentPane;
	}

	/** Returns an ImageIcon, or null if the path was invalid. */
	protected ImageIcon createImageIcon(String path) {
			return new ImageIcon(path);
	}
}

