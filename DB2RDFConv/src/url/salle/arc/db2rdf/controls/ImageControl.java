package url.salle.arc.db2rdf.controls;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ImageControl {
	public static JPanel createIcon(String loFilePath, FlowLayout aoFlowLayout) {
		JPanel loJPanel = new JPanel(aoFlowLayout);
		ImageIcon loIcon = new ImageIcon(loFilePath);
		Image image = loIcon.getImage(); // transform it 
		Image newimg = image.getScaledInstance(60, 60,  java.awt.Image.SCALE_SMOOTH);  
		ImageIcon imageIcon = new ImageIcon(newimg); 
		loJPanel.add(new JLabel(imageIcon, JLabel.LEFT));
		loJPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		//loJPanel.setBackground(Color.cyan);		
		return loJPanel;
	}	
}
