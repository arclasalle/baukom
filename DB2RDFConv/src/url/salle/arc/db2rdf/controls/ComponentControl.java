package url.salle.arc.db2rdf.controls;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

public class ComponentControl {
	
	public static GridBagConstraints addGrid(GridBagLayout aoGBL, int loGC, double adWeightx, Insets loInsets) {
		GridBagConstraints loGBC = new GridBagConstraints();
		loGBC.fill = loGC;
		loGBC.weightx = adWeightx;
		if(loInsets == null){
			loGBC.insets = Global.INSETS_PANEL_DEFAULT;
		} else {
			loGBC.insets = loInsets;
		}
		return loGBC;
	}
	
   public static Component addGrid(GridBagLayout aoGBL, Component aoComponent, int loGC, int aiGridx, int aiGridy, int aiGridWidth, int aiGridHeight,
			double adWeightx, double adWeighty, Insets loInsets) {
    	
		GridBagConstraints loGBC = new GridBagConstraints();
		loGBC.fill = loGC;
		loGBC.gridx = aiGridx;
		loGBC.gridy = aiGridy;
		loGBC.gridwidth = aiGridWidth;
		loGBC.gridheight = aiGridHeight;
		loGBC.weightx = adWeightx;
		loGBC.weighty = adWeighty;
		if(loInsets == null){
			loGBC.insets = Global.INSETS_PANEL_DEFAULT;
		} else {
			loGBC.insets = loInsets;
		}
		aoGBL.setConstraints(aoComponent, loGBC);
		
		return aoComponent;
	}  
}
