package url.salle.arc.db2rdf;

import java.awt.Component;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

class CustomeTreeCellRenderer extends DefaultTreeCellRenderer {

	public CustomeTreeCellRenderer() {
	}

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
			boolean leaf, int row, boolean hasFocus) {

		super.getTreeCellRendererComponent(tree, value, leaf, expanded, leaf, row, hasFocus);
		CustomTreeNode node = (CustomTreeNode) value;

		setClosedIcon(node.getIcon());
		setOpenIcon(node.getIcon());
		
		return this;
	}
}