package url.salle.arc.db2rdf;

public class Connection {	
	public static String Domain = ""; // ex: 192.168.0.192, localhost, ...
	public static String Instance = ""; // ex: INSTANCE\\SQLEXPRESS
	public static String DefaultDBName = ""; // ex: Baukom_Catalogue
	public static String Port = ""; // ex: 1433
	public static String Driver = ""; // ex: jdbc:sqlserver
	public static String UserName = ""; // admin
	public static String UserPasword = ""; // ***
	public static void formatInstanceName() {
		if (Instance.contains("\"") && !Instance.contains("\\")) {
			Instance.replace("\"", "\\");
		}
	}
}


