package url.salle.arc.db2rdf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.swing.JTextArea;

public class UtilsApp {

	private static String msError = "";
	public static boolean ExecuteProc(String asCommand, JTextArea moTAOutput){
		try {
			long start_time = System.nanoTime();
			
			Process proc = Runtime.getRuntime().exec(asCommand);
			BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
			String lsline = null;					
			
			if ((lsline = in.readLine()) != null) {		        
		        System.out.print(lsline);					        
			}
			else {
				
				long end_time = System.nanoTime();
				double difference = (end_time - start_time)/1000000000.0;
				DecimalFormat df = new DecimalFormat();
				df.setMaximumFractionDigits(2);				
				moTAOutput.append("executed in " + df.format(difference) + " seconds\n");
				
				stdError.lines().forEach(s -> msError+= s+"\n");
				if(msError.equals("")){
					return true;
				} else {
					moTAOutput.append(msError+"\n");
					System.out.println(msError);
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return false;		
	}
	
	public static String[] getUpperOntologiesFileNames(String asDirectory){
		List<String> mLstUOFileNames = new ArrayList<String>();
		final File folder = new File(asDirectory);
		for (final File fileEntry : folder.listFiles()) 
		{
//	    		byte[] loContent = Files.readAllBytes(Paths.get(lsDirectory + "\\" + fileEntry.getName()));
//	    		lsQuery = new String(loContent, Charset.defaultCharset());
//				String[] lsQueryArrayTemp = Arrays.copyOf(lsQueryArray, lsQueryArray.length+1);
//	    		lsQueryArrayTemp[lsQueryArray.length] = lsQuery;
//	    		lsQueryArray = lsQueryArrayTemp;
			mLstUOFileNames.add(fileEntry.getName());
		}
		return mLstUOFileNames.toArray(new String[0]);
	}	
	
	
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:sqlserver://PC-GONS\\SQLEXPRESS";

	// Database credentials
	static final String USER = "admin";
	static final String PASS = "arc2k3";

	public static void testConnectionToDataBase() {

		java.sql.Connection conn = null;
		Statement stmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			
			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			
			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = conn.createStatement();
			/*
			String sql;
			sql = "SELECT id, first, last, age FROM Employees";
			ResultSet rs = stmt.executeQuery(sql);

			// STEP 5: Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				int id = rs.getInt("id");
				int age = rs.getInt("age");
				String first = rs.getString("first");
				String last = rs.getString("last");

				// Display values
				System.out.print("ID: " + id);
				System.out.print(", Age: " + age);
				System.out.print(", First: " + first);
				System.out.println(", Last: " + last);
			}
			// STEP 6: Clean-up environment
 			rs.close();
			*/
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try
		System.out.println("Goodbye!");
	}// end main
}// end FirstExample
	

