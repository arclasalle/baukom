package url.salle.arc.db2rdf;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

class TreeRenderer extends DefaultTreeCellRenderer {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Icon closed = (Icon) UIManager.get("InternalFrame.maximizeIcon");
    private static final Icon open = (Icon) UIManager.get("InternalFrame.minimizeIcon");

    @Override
    public Component getTreeCellRendererComponent(
    		JTree tree, 
    		Object value,
    		boolean sel,
    		boolean exp,
    		boolean leaf,
    		int row,
    		boolean hasFocus) {
    	super.getTreeCellRendererComponent(tree, value, sel, exp, leaf, row, hasFocus);
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
        setOpenIcon(open);
        setClosedIcon(closed);
        return this;
    }
}