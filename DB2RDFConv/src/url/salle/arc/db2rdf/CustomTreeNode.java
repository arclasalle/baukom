package url.salle.arc.db2rdf;


import javax.swing.ImageIcon;
import javax.swing.tree.DefaultMutableTreeNode;

public class CustomTreeNode extends DefaultMutableTreeNode {

  /**
   * The icon which is displayed on the JTree object. open, close, leaf icon.
   */
  private ImageIcon icon;
  private Object object;

  public CustomTreeNode(ImageIcon icon) {
    this.icon = icon;
  }

  public CustomTreeNode(ImageIcon icon, String asName, Object userObject) {
    super(asName);
    this.icon = icon;
    this.object = userObject;
  }

  public CustomTreeNode(ImageIcon icon, String asName, Object userObject, boolean allowsChildren) {
    super(userObject, allowsChildren);
    this.icon = icon;
    this.object = userObject;
  }

  public ImageIcon getIcon() {
    return icon;
  }

  public void setIcon(ImageIcon icon) {
    this.icon = icon;
  }    
  
  public Object getObject() {
	    return object;
  }
}