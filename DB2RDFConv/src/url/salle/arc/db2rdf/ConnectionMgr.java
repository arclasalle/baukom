package url.salle.arc.db2rdf;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.thoughtworks.xstream.XStream;

public class ConnectionMgr {
	
	public static void loadConnectionConfig() {
		ArrayList<String> llstConnection = new ArrayList<String>();
		try {
			org.w3c.dom.Document doc = null;
			File f = new File("config.xml");
			if (f.exists() && !f.isDirectory()) {
				XMLDecoder decoder = null;
				try {
					decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream("config.xml")));
				} catch (FileNotFoundException e) {
					System.out.println("ERROR: File dvd.xml not found");
				}
				// Load connection values:
				ConnectionSerialize loConnection = (ConnectionSerialize)decoder.readObject();
				Connection.Domain = loConnection.Domain;
				Connection.Instance = loConnection.Instance;
				Connection.DefaultDBName = loConnection.DefaultDBName;
				Connection.Port = loConnection.Port;
				Connection.Driver = loConnection.Driver;
				Connection.UserName = loConnection.UserName;
				Connection.UserPasword = loConnection.UserPasword;
			}		
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}
	
	public static boolean saveConnectionConfig() {
		ConnectionSerialize loTmpConnection = new ConnectionSerialize();
		loTmpConnection.Domain = Connection.Domain;
		loTmpConnection.Instance = Connection.Instance;
		loTmpConnection.DefaultDBName = Connection.DefaultDBName;
		loTmpConnection.Port = Connection.Port;
		loTmpConnection.Driver = Connection.Driver;
		loTmpConnection.UserName = Connection.UserName;
		loTmpConnection.UserPasword = Connection.UserPasword;
		try {
			XMLEncoder encoder = null;
			try {
				encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("config.xml")));
			} catch (FileNotFoundException fileNotFound) {
				System.out.println("ERROR: While Creating or Opening the File dvd.xml");
			}			
			encoder.writeObject(loTmpConnection);
			encoder.close();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return true;
	}
}
