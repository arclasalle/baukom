package es.url.salle.arc.chkservice;

import java.io.BufferedReader;

/**
 * author: Gon�al Costa (gcosta@salleurl.edu)
 * supervisor: Pieter Pauwels (pipauwel.pauwels@ugent.be)
 * 
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.rdf.model.InfModel;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Selector;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;

import com.hp.hpl.jena.reasoner.Reasoner;
import com.hp.hpl.jena.reasoner.TriplePattern;
import com.hp.hpl.jena.reasoner.rulesys.GenericRuleReasonerFactory;
import com.hp.hpl.jena.reasoner.rulesys.Rule;
import com.hp.hpl.jena.util.FileManager;
import com.hp.hpl.jena.vocabulary.ReasonerVocabulary;

public class InferenceMgr 
{
	// Members:
    Reasoner reasoner;      	// Reasoner
    Model moModelBIM;			// BIM model in RDF
    Resource configuration;		// A resource used to configure the engine
    InfModel infModel;			// the inference model
    Builtin builtin;  
    String msPropertyToCheck;
    String msPropertyToCheckPrefixName;
    
    // Constructor:
    public InferenceMgr() 
    {
    	this.reasoner = null;
    	this.moModelBIM = null;
    	this.configuration = null;
    	this.infModel = null;
    	
    	builtin = new Builtin();   	
    }
    
    // Description:
    // ...
    public boolean checkModel( 	String asBIMFilePath, 					// File 1 (input): contains the BIM model.												
								String asRuleFilePath,					// File 2 (input): contains the inference rules.
								String asBIMOutFilePath,				// File 3 (output): output file name.
	    						String asBIMFileIntermediateForTesting, // File 4 (output): will contains an intermediate merging model (BIM-Regulation) for testing.
	    						String [] asComponentsToCheckClassNames,//
	    						String [][] alstlstComponentsToCheckPropertyTypes, //
	    						String [][] alstlstComponentsToCheckProperties, 
	    						String [][] alstlstProductParametersValues,	// e.g. "16.0","2.695"
	    						String asSuggestionProductPrefixUri,	// 
	    						String asSuggestionProductPrefixName,	// 
	    						String asSuggestProperty,				// e.g. "hasSuggestedProducts"
			    				int aiNumAlternatives) 
    {
    	// Merge BIM and regulation models and include the rule in the configuration of the reasoner:
		initReasoner(asRuleFilePath, asBIMFilePath, asBIMFileIntermediateForTesting, asSuggestionProductPrefixUri,
				asSuggestionProductPrefixName, true);
    	
    	// Get instances of the component(s) defined in the BIM model ----------------------------- 
    	
    	String lsBaseUri = "";
    	List<List<Resource>> llstlstComponentInstances = new ArrayList<List<Resource>>();
		StmtIterator iter = moModelBIM.listStatements();
		
		// for each component class name:
		for (int i=0; i<asComponentsToCheckClassNames.length; i++)
		{
			List<Resource> llstComponentInstances = new ArrayList<Resource>();
			while (iter.hasNext())  
			{
				Statement stmt = iter.nextStatement();
				if(stmt.getObject().isResource())
				{
					if(((Resource)stmt.getObject()).getLocalName().equals(asComponentsToCheckClassNames[i])){						
						llstComponentInstances.add(stmt.getSubject());
						if(lsBaseUri.equals("")) {
							lsBaseUri = ((Resource)stmt.getObject()).getNameSpace();
						}
					}
				}
			}			
			llstlstComponentInstances.add(llstComponentInstances);
		}
		
		List<List<Property>> llstlstParamProperty = new ArrayList<List<Property>>();
    	for (String[] lsParam : alstlstComponentsToCheckProperties)	
    	{
    		List<Property> llstParamProperty = new ArrayList<Property>();
    		for (int i=0; i < lsParam.length ; i++) {
	    		llstParamProperty.add(moModelBIM.getProperty(lsBaseUri + lsParam[i]));
    		}
    		llstlstParamProperty.add(llstParamProperty);
    	}		
		
		// ----------------------------------------------------------------------------------------
		// STEP 1: check for all instances of the type of component(s) selected
		// ----------------------------------------------------------------------------------------
		
		// INFERENCE ------------------------------------------------------------------------------
		
    	infModel = ModelFactory.createInfModel(reasoner, moModelBIM); // create the inference model
    	infModel.prepare();
    	
    	Property loPropertyToCheck = moModelBIM.getProperty(msPropertyToCheck);
		// e.g. asPropertyToCheck = cte_EHE08:50221_MinHeightCheck		
		
		for(int liTypeComponents=0; liTypeComponents < llstlstComponentInstances.size(); liTypeComponents++)
		{
			// for all instances:
			for(int instance=0; instance < llstlstComponentInstances.size(); instance++)
	    	{
				Resource loComponentInstance_uri = llstlstComponentInstances.get(liTypeComponents).get(instance);
			    	
				// Checks if there is an inferred value in the THEN-part for the current instance of the component:
				// e.g. rvt:hcSlab_Dummy_001  cte_EHE08:MinimumHeightBorder  "value"		    		
				Selector loSelector = new SimpleSelector(loComponentInstance_uri, loPropertyToCheck, (RDFNode) null);
		    	StmtIterator loIter = infModel.listStatements(loSelector);
		    	if (loIter.hasNext()) {
		    		System.out.println("Rule satisfied for the component: " + loComponentInstance_uri);
				} else {
		    		System.out.println("Rule NOT satisfied for the component: " + loComponentInstance_uri);
		    		// Note: if the rule is not satisfied, we know nothing (we can not infer anything)
		    	}
	    	}
		}

		// ----------------------------------------------------------------------------------------
		// STEP 2: suggest products for all "ascertainable" instances of the type of component(s) selected
		// ----------------------------------------------------------------------------------------

		// IMPORTANT: to assert that a regulation is met or not, we need two rules 
    	//            in which the condition (positive and negative) must be satisfied. 
		
		for(int liComponentType=0; liComponentType < llstlstComponentInstances.size(); liComponentType++)
		{
			// for all instances:
			for(int instance=0; instance < llstlstComponentInstances.get(liComponentType).size(); instance++)
	    	{
				Resource loComponentInstance_uri = llstlstComponentInstances.get(liComponentType).get(instance);
							
				// DECISION: all the parameters need to be defined in the original instance to suggest products!
				
				Selector loSelector = new SimpleSelector(loComponentInstance_uri, loPropertyToCheck, (RDFNode) null);
				StmtIterator loIter = infModel.listStatements(loSelector);
				
				// if the current instance meet with the rule (e.g. "meet with the regulation XXX" or "NOT meet with the regulation XXX"):
		    	if (loIter.hasNext()) 
				{
					// STEP 2.1 add suggestion of products: 
					addProductSuggestions(	 loComponentInstance_uri,
											 loPropertyToCheck,
											 asSuggestProperty,
											 alstlstProductParametersValues,											 
											 alstlstComponentsToCheckPropertyTypes[liComponentType],
											 llstlstParamProperty.get(liComponentType),
											 aiNumAlternatives);
				}
	    	}
		}
		
		printModel(asBIMOutFilePath, infModel);
		
		return true;
    }
    
    // Method - Merge BIM and regulation models and include the rule to the configuration of the reasoner.
    public void initReasoner(String asRegulationFile,
				    		 String asBIMFile,
				    		 String asBIMFileIntermediateForTesting,
				    		 String asSuggestionProductPrefixUri,
    						 String asSuggestionProductPrefixName,
				    		 boolean abGenerateIntermediateModel)
    {
    	// Parse checking property ----------------------------------------------------------------
    	
    	String lsPropertyToCheckPrefixUri = getCheckingProperty(asRegulationFile);
    	if(lsPropertyToCheckPrefixUri.equals(""))
    	{
    		System.out.println("Error: there is no checking property in the THEN clause of the rule");
            System.exit(0);
    	}

    	// create the RDF model and the configuration for the reasoner:
    	moModelBIM = FileManager.get().loadModel(asBIMFile);
    	configuration = moModelBIM.createResource();
    	configuration.addProperty(ReasonerVocabulary.PROPruleMode, "hybrid");
    	configuration.addProperty(ReasonerVocabulary.PROPruleSet, asRegulationFile);
   		reasoner = GenericRuleReasonerFactory.theInstance().create(configuration);
    	
    	// Add prefixes:
    	moModelBIM.setNsPrefix(msPropertyToCheckPrefixName, lsPropertyToCheckPrefixUri);
    	moModelBIM.setNsPrefix(asSuggestionProductPrefixName, asSuggestionProductPrefixUri);
    	
    	// Generate intermediate model (for testing) ----------------------------------------------
    	
    	if(abGenerateIntermediateModel)
    	{
	    	FileOutputStream ont1;
	    	try 
	    	{
	    		if(moModelBIM != null)
	    		{
	    			// generate 
	    			File f = new File(asBIMFileIntermediateForTesting);    		        
	    			ont1 = new FileOutputStream(f);
	    			moModelBIM.write(ont1, "TTL");
	    			ont1.close();	    		        
	    		}
	    	} 
	    	catch (Exception e) { e.printStackTrace(); }
    	}
    	
    	// ----------------------------------------------------------------------------------------
    }
    
    /*
    // Method - create and execute the rule engine
    public void infersMinimumHeightBorder(	String asUriModel,
    										String asRuleFile, 
				    						String asBimFile, 
				    						String asBIMFile, 
				    						String asOutputRDFFile,
				    						String asRegulationFile) 
    {
    	// Connect to an EndPoint to retrieve the product information:   
    	QueryCatalogue loQueryCatalogue = new QueryCatalogue("localhost:8890", "");
    	String [][] lMtxHollowCoreSlabs = loQueryCatalogue.getProducts();
    
    	// Init objects and load files:
    	//MergeModels(asRuleFile, asBimFile, asRegulationFile, asBIMFile, true);
    	
    	Resource HCSlabHelper = moModelBIM.getResource(asUriModel + "HCSlabHelper"); 
		Property lpHeight = moModelBIM.getProperty(asUriModel + "height");
		
    	// For each Slab helper, we iterate it:
		
		StmtIterator iter = moModelBIM.listStatements();
		while (iter.hasNext()) 
		{
			Statement stmt = iter.nextStatement();  // get next statement
		    if(stmt.getObject().equals(HCSlabHelper))
		    {
		    	Resource lrSlabIndRes = stmt.getSubject();
		    	
		    	boolean lbIsSlabHeightDefined = loQueryCatalogue.isDefined(moModelBIM, lrSlabIndRes, lpHeight);
		    	if(lbIsSlabHeightDefined)
		    	{
		    		infModel = ModelFactory.createInfModel(reasoner, moModelBIM);	// create the inference model
		        	infModel.prepare();												// force starting the rule execution
		    	}
		    	else
		    	{
		    		for(int row=0; row<lMtxHollowCoreSlabs.length; row++)
			    	{
		    			if(row > 0)
		    				moModelBIM.remove(lrSlabIndRes, lpHeight, moModelBIM.createTypedLiteral(new Double(Double.parseDouble(lMtxHollowCoreSlabs[row-1][0])*100.0 ))); //String.valueOf(lMtxHollowCoreSlabs[row][0]*100.0 )+ "\"^^xsd:double"));
		    			
		    			moModelBIM.addLiteral(lrSlabIndRes, lpHeight, Double.parseDouble(lMtxHollowCoreSlabs[row][0])*100.0);
		    			
		    			infModel = ModelFactory.createInfModel(reasoner, moModelBIM);	// create the inference model
			        	infModel.prepare();	
			        	
		    			FileOutputStream ont2;
		    			try 
		    			{
		    				if(infModel != null)
		    				{
		    					File f = new File(asOutputRDFFile);    		        
		    				    ont2 = new FileOutputStream(f);
		    				    infModel.write(ont2, "TTL");
		    				    ont2.close();	    		        
		    				}
		    			} 
		    			catch (Exception e)	{ e.printStackTrace();}
			    	}
		    	}
		    }
		}
    }
    */
    
    // Method - Suggest products
    private void addProductSuggestions(	Resource aoComponentInstance_uri,
    									Property aoPropertyToCheck,
    									String aoSuggestProperty,
										String [][] asProductParametersValues,
										String [] alstParamValueType,
										List<Property> alstParamProperty,
										int aiNumAlternatives)
    {
    	// Iterate for all properties defined in the config. file which must be correspond and
    	// must be defined in the same order than the properties retrieved from each product of
    	// the catalogue:

    	// PROCESS:
    	// 1. Save the original values of the properties of the current instance
    	// ITERATE for each property of the product defined in the instance (should be the same):
    	//  2. Overwrite values of existing properties with the ones of the current product.
    	//  3. Infer
    	//  3. If the component meet, insert the product as a suggestion (list of products provided should be sorted)
    	// END
    	// 4. Restore original values of the instance 
    	
    	// ----------------------------------------------------------------------------------------
    	// STEP 1: save the context of the instance verified by the rule. This involves  
    	//         the original statements corresponding to each property and the value checked.
    	//         Both will be rewrite.
    	// ----------------------------------------------------------------------------------------
    	
    	Statement[] llstoOriginalPropertyStatement = new Statement[alstParamProperty.size()]; 
		Selector loSelector = null;
		StmtIterator loIter = null;
    	
    	// for each property of the product:
		for(int liNumProperty = 0; liNumProperty < alstParamProperty.size(); liNumProperty++)
    	{
			// Search properties for the current instance (e.g. height, weight):
			
			// e.g. rvt:hcSlab_Dummy_002     rvt:height     19.0^^http://www.w3.org/2001/XMLSchema#double (default values)
			// e.g. rvt:hcSlab_Dummy_002     rvt:weight     0.0^^http://www.w3.org/2001/XMLSchema#double (default values)
			
			Property loProperty = alstParamProperty.get(liNumProperty);
			loSelector = new SimpleSelector(aoComponentInstance_uri, loProperty, (RDFNode) null);
			//loIter = infModel.listStatements(loSelector);
			loIter = moModelBIM.listStatements(loSelector);
	    	if (loIter.hasNext())
	    	{
	    		// save the original statement for the current property:
	    		llstoOriginalPropertyStatement[liNumProperty] = loIter.nextStatement();
	    		System.out.println("Property saved: " + llstoOriginalPropertyStatement[liNumProperty].toString());
	    	}
    	}
		
    	// ----------------------------------------------------------------------------------------
    	// STEP 2: Rewrite the property with the values of the properties of the products retrieved 
		//         from the catalogue.
    	// ----------------------------------------------------------------------------------------
		
		int liNumAlternativesCurrent = aiNumAlternatives;
		
    	// for each instance of product retrieved from the catalogue:
    	for(int aiNumProduct = 0; aiNumProduct < asProductParametersValues.length; aiNumProduct++)
    	{
    		if(liNumAlternativesCurrent > 0)
    		{
	    		// STEP 2.1: Rewrite properties -------------------------------------------------------
	    		
		    	// for each property of the product:
				for(int liNumProperty = 0; liNumProperty < alstParamProperty.size(); liNumProperty++)
		    	{
			    	// IMPORTANT: properties of products must be the same that those already defined in the instance! 
			    	
					Property loProperty = alstParamProperty.get(liNumProperty); // e.g. rvt:height
			    	Literal lPropertyValueType = null;
			    	
		    		// Remove the original values of the property, or those rewritten by previous product:
		    		loSelector = new SimpleSelector(aoComponentInstance_uri, loProperty, (RDFNode) null);
					loIter = infModel.listStatements(loSelector);
					if (loIter.hasNext()) {
						moModelBIM.remove(loIter.nextStatement());
					}
		    		
					if(alstParamValueType[liNumProperty].equals("string")) { 
						lPropertyValueType = moModelBIM.createTypedLiteral(new String(asProductParametersValues[aiNumProduct][liNumProperty]));
					}
					else if(alstParamValueType[liNumProperty].equals("float")) {
						lPropertyValueType = moModelBIM.createTypedLiteral(new Float(asProductParametersValues[aiNumProduct][liNumProperty]));
					}
					else if(alstParamValueType[liNumProperty].equals("integer")) {
						lPropertyValueType = moModelBIM.createTypedLiteral(new Integer(asProductParametersValues[aiNumProduct][liNumProperty]));
					}
					else if(alstParamValueType[liNumProperty].equals("double")) {
						lPropertyValueType = moModelBIM.createTypedLiteral(new Double(asProductParametersValues[aiNumProduct][liNumProperty]));
					}
					
					// Add the property in the current instance:			    			
					moModelBIM.addLiteral(aoComponentInstance_uri, loProperty, lPropertyValueType);
		    	}
			
	    		// STEP 2.1: Infer results ------------------------------------------------------------
				
				// ERROR -> we are inferring over all the model -> this is not efficient!
				
				infModel = ModelFactory.createInfModel(reasoner, moModelBIM);	// REWRITE the inference model! The last instance is the only that will be rewrite???
		    	infModel.prepare();
		    	
		    	// Note: previous inferences are not included in the variable moModelBIM.
		    	
				// Checks if there is an inferred value in the THEN-part for the current instance of the component:
				// e.g. rvt:hcSlab_Dummy_001  cte_EHE08:MinimumHeightBorder  "value"		    		
				loSelector = new SimpleSelector(aoComponentInstance_uri, aoPropertyToCheck, (RDFNode) null);
		    	loIter = infModel.listStatements(loSelector);
		    	if (loIter.hasNext()) 
		    	{
		    		Statement loST = loIter.nextStatement();
		    		Literal loValue = loST.getLiteral();		    		
		    		Literal loValueTrue = infModel.createTypedLiteral(new Boolean(true));
		    		if(loValue.equals(loValueTrue))
		    		{
		    			// Add the product reference:
			    		Property loSuggestProperty = moModelBIM.getProperty(aoSuggestProperty+(aiNumAlternatives - liNumAlternativesCurrent + 1));
			    		Resource loProductResource_uri = moModelBIM.createResource(asProductParametersValues[aiNumProduct][asProductParametersValues[aiNumProduct].length - 1]);
			    		moModelBIM.add(aoComponentInstance_uri, loSuggestProperty, loProductResource_uri);
			    		System.out.println("Product check: Rule satisfied for component " + aoComponentInstance_uri + " product: " + asProductParametersValues[aiNumProduct][alstParamProperty.size()-1]);
			    		liNumAlternativesCurrent--;
		    		} else	{ /* No valid product */ }
				}
		    	else 
		    	{
		    		System.out.println("Product check: Rule NOT satisfied for the component: " + aoComponentInstance_uri + " product: " + asProductParametersValues[aiNumProduct][alstParamProperty.size()-1]);
		    		// Note: if the rule is not satisfied, we know nothing (we can not infer anything)
		    	}
    		}
    	}
    	
    	// ----------------------------------------------------------------------------------------
    	// STEP 3: 
    	// ----------------------------------------------------------------------------------------
    	
		// restore original properties of the instance:
		for(int liNumProperty = 0; liNumProperty < alstParamProperty.size(); liNumProperty++)
		{
			Property lParamProperty = alstParamProperty.get(liNumProperty);
			loSelector = new SimpleSelector(aoComponentInstance_uri, lParamProperty, (RDFNode) null);
			loIter = moModelBIM.listStatements(loSelector);
	    	if (loIter.hasNext()) 
	    	{
	    		// Remove values form the property of products:
    			moModelBIM.remove(loIter.nextStatement());	// Remove current property  			
    			moModelBIM.add(llstoOriginalPropertyStatement[liNumProperty]);				// Add original property
	    	}
		}
    }
    
    private String getCheckingProperty(String asRuleFile)
    {
    	try 
    	{
    		BufferedReader loBR = new BufferedReader(new FileReader(asRuleFile));
    		List rules = Rule.parseRules(Rule.rulesParserFromReader(loBR));    		
    	    for (Iterator<Rule> i = rules.iterator(); i.hasNext(); ) 
    	    {
    	        Rule loRule = i.next();    	        
    	        Object[] head = loRule.getHead();    	        
    	        for (int j = 0; j < head.length; j++) 
    	        {
    	        	if (head[j] instanceof TriplePattern) 
    	        	{
    	                Node loObject = ((TriplePattern) head[j]).getObject();
    	                if(loObject.isLiteral())
    	                {
    	                	if(	loObject.getLiteralValue().toString().equals("true") ||
    	                		loObject.getLiteralValue().toString().equals("false"))
    	                	{
    	                		Node loPredicate = ((TriplePattern) head[j]).getPredicate();
    	                		msPropertyToCheck = loPredicate.getURI();
    	                		String lsNameSpace = loPredicate.getNameSpace();
    	                		if(lsNameSpace.contains("#")){
    	                			lsNameSpace = lsNameSpace.split("#")[0];
    	                		}
    	                		msPropertyToCheckPrefixName = getPrefixName(asRuleFile, lsNameSpace);
    	                		return lsNameSpace + "#";
    	                	}
    	                }
    	            }
    	        }
    	    }
		} catch (FileNotFoundException fne) {
			System.out.println("Error: File Not Found" + fne);
		}
    	return "";
    }
    
    // PRINT METHODS ------------------------------------------------------------------------------
    
    // Method - Print the content of the model in Turtle format.
    private void printModel(String asOutputFile, Model aoModel)
    {
    	FileOutputStream ont2;
		try 
		{
			if(infModel != null)
			{
				// The name of the file generated follows this convention: path + filename + "_output.ttl"
				File f = new File(asOutputFile);    		        
			    ont2 = new FileOutputStream(f);
			    aoModel.write(ont2, "TTL");
			    ont2.close();	    		        
			}
		} 
		catch (Exception e)	{ e.printStackTrace();}
    }
    
    // Method -
    private void printInstance(Resource aoComponentInstance_uri)
    {
    	System.out.println("Instance " + aoComponentInstance_uri);
    	StmtIterator loIter = aoComponentInstance_uri.listProperties(); // get all statements
    	while (loIter.hasNext()) {
			System.out.println("Prop: "+ loIter.nextStatement().toString());
    	}
    } 
    
    private String getPrefixName(String asFileName, String asPrefixUri)
    {
    	try 
    	{
    		String lsPrefixUriFormated = asPrefixUri;
    		if(lsPrefixUriFormated.contains("#")) {
    			lsPrefixUriFormated = lsPrefixUriFormated.split("#")[0];
			}
    		
    		FileReader loInput = new FileReader(asFileName);    		
    		BufferedReader loBufRead = new BufferedReader(loInput);
        	String lsLine = null;
        	
        	while ( (lsLine = loBufRead.readLine()) != null)
        	{    
        		if(lsLine.contains("@prefix")) 
	    		{
        			String lsPrefix = lsLine.split(":", 2)[1].replace(" ","").replace("\t","");
        			if(lsPrefix.contains("#")) {
        				lsPrefix = lsPrefix.split("#")[0];
        			}
        			
        			if(lsPrefix.equals(lsPrefixUriFormated))
        			{
        				loBufRead.close();
        				return lsLine.split(":")[0].replace("@prefix", "").replace(" ","").replace("\t","");
        			}
	    		}
        	}        	
        	loBufRead.close();
    	} catch (FileNotFoundException fne) {
    		System.out.println("Error: File Not Found" + fne);
		} catch (IOException ioe) {
			System.out.println("Error: IOException" + ioe);
		}     	
    	return "";
    }
    
    // Method -
    private void printAllInstance(	String asModelUri, 
    								String [] asInstanceListClassNames,
    								Model aoModel)
    {
		StmtIterator iter = aoModel.listStatements();
		// for all statements of the model:
		while (iter.hasNext())  
		{
			// Filter instances of the type(s) of element(s) selected (e.g. instances of hollow core slab components):
			Statement stmt = iter.nextStatement();		
			// for each component class name:
			for (int i=0; i<asInstanceListClassNames.length; i++){
				if(stmt.getObject().toString().equals(asModelUri + asInstanceListClassNames[i])){
					printInstance(stmt.getSubject());
				}
			}
		}
    }
}