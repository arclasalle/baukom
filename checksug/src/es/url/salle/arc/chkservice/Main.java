package es.url.salle.arc.chkservice;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.io.*;

/**
 * author: Gon�al Costa (gcosta@salleurl.edu)
 * supervisor: Pieter Pauwels (pipauwel.pauwels@ugent.be)
 * 
 */

import org.apache.log4j.BasicConfigurator;

public class Main extends Object 
{
	private static String msUrlCatalogue = "";
	public static String[] msComponentsToCheckClassNames; 		// e.g. HCSlabHelper
	public static String[][] msComponentsToCheckProperties;		// e.g. height, weight
	public static String[][] msComponentsToCheckPropertyTypes;	// e.g. double, double
	public static String msSuggestionProductPrefixUri;			// e.g. http://www.suggestion_products.org/ontology/#  
	public static String msSuggestionProductPrefixName;			// e.g. ...
	public static String msSuggestionProductProperty;			// e.g. hasSuggestedProducts
	public static int miNumAlternatives = 1;
	
	public static void main(String args[]) {
		try {
			
    		LoadConfigurationFile();		// Read the configuration.
	    	BasicConfigurator.configure();	
	    	
			if (args.length != 3) {
				System.out.println("Usage:");
				System.out.println(" java -jar checksug.jar 'bim_model_in.ttl' 'rule.rule' 'bim_model_out.ttl'");
				System.exit(0);
			}
	    	
	    	String lsBIMFilName = args[0];
	    	String lsRuleFileName = args[1];
	    	String lsBIMFilOutName = args[2];
	    	
			// checks:
			if (msUrlCatalogue.equals("")) {
				System.out.println("Error: 'url_catalogue' parameter has not been properly defined in the config file");
				System.exit(0);
			}
	    		    	
	    	InferenceMgr loInferenceMgr = new InferenceMgr();
	    	
	    	// Retrieve products from the catalogue ------------------------------------------------
	    	
	    	String lsQuery = loadSPARQLQueryFile();	// Load query from a file.
	    	
	    	// Connect to a product data source (catalogue) to retrieve the information of products:
	    	String [][] lstsProductsCatalogue = null;
	    	QueryCatalogue loQueryCatalogue = new QueryCatalogue(msUrlCatalogue, lsQuery);
	    	
	    	// Get a list of products from the catalogue:
	    	lstsProductsCatalogue = loQueryCatalogue.getProducts();
	    		    
			// Checks that the product list is not empty:
			if (lstsProductsCatalogue == null) {
				System.out.println("Error: the product list is empty.");
				System.exit(0);
			} 	    	
	    	
	    	// IMPORTANT: parameters retrieved from the catalogue need to be provided in the seem order than in 
	    	// the "component_1_properties" parameter specified in the configuration file:
	    	// for example: {"16.0", "2.695", "http://www.baukomcatalogue.org/companies/precat/products/LP16"},
	    	//               height, weight
	    	
	    	// Formalize URI properties  ----------------------------------------------------------
	    	
	    	// Create an URI for each property to add in each instance of the component of the model (if not exist)
	    	
			if (msComponentsToCheckProperties.length != msComponentsToCheckPropertyTypes.length) {
				System.out.println(
						"Error: Not all property lists of the components have a corresponding list of their types or vice versa.");
				System.exit(0);
			}

			if ((msComponentsToCheckClassNames == null) || (msComponentsToCheckClassNames.length == 0)) {
				System.out.println("Error: No class name for the component to check has been defined.");
				System.exit(0);
			}
	    
	    	// ------------------------------------------------------------------------------------
	    	
	    	// Property to add products in the THEN part of the rule:
	    	String lsPropertySuggestion = msSuggestionProductPrefixUri + msSuggestionProductProperty;
	    	// e.g. "http://www.suggestion_products.org/ontology/#hasSuggestedProducts"	    	
	    	
	    	String lsBIMtestingFilName = "";
	    	int i = lsBIMFilName.lastIndexOf('.');
	    	if (i > 0) {
	    		lsBIMtestingFilName = lsBIMFilName.substring(0,i) + "_for_testing.ttl";
	    	}
	    	
	    	boolean lbResult = loInferenceMgr.checkModel(
	    			lsBIMFilName,
	    			lsRuleFileName, 
	    			lsBIMFilOutName,
	    			lsBIMtestingFilName,
	    			msComponentsToCheckClassNames,
	    			msComponentsToCheckPropertyTypes,
	    			msComponentsToCheckProperties,
	    			lstsProductsCatalogue,
	    			msSuggestionProductPrefixUri,
	    			msSuggestionProductPrefixName,
	    			lsPropertySuggestion,
	    			miNumAlternatives);
	    	
	    	if(lbResult) {
	    		System.out.println("Rule done");	            
	    	} else {
	    		System.out.println("Error: the operation has failed.");
	    	}
    	}
    	catch (Exception e)	{ e.printStackTrace();}
    }
    
	// Method - Read the configuration file.
	public static void LoadConfigurationFile() {
		try {
			FileReader loInput = new FileReader("files/config.cfg");
			BufferedReader loBufRead = new BufferedReader(loInput);
			String lsLine = null;
			int liNumComponents = 0;
			msComponentsToCheckProperties = new String[0][0];
			msComponentsToCheckPropertyTypes = new String[0][0];

			while ((lsLine = loBufRead.readLine()) != null) {
				String[] array1 = lsLine.split(":", 2);

				if (array1[0].equals("url_catalogue")) {
					msUrlCatalogue = array1[1];
				} else if (array1[0].equals("component_" + (liNumComponents + 1) + "_class_name")) {
					msComponentsToCheckClassNames = array1[1].split(" ");
					List<String> msElementsToCheckClassNames_temp = new ArrayList<String>();

					int liNUm = msComponentsToCheckClassNames.length;
					for (int i = 0; i < liNUm; i++) {
						if ((!msComponentsToCheckClassNames[i].equals(""))
								&& (!msComponentsToCheckClassNames[i].equals(" "))) {
							msElementsToCheckClassNames_temp.add(msComponentsToCheckClassNames[i].replaceAll("\"", ""));
						}
					}
					msComponentsToCheckClassNames = msElementsToCheckClassNames_temp
							.toArray(new String[msElementsToCheckClassNames_temp.size()]);
					liNumComponents++;
				} else if (array1[0].equals("component_" + liNumComponents + "_properties")) {
					// About these properties:
					// - Properties that corresponds with the ones retrieved from the products of
					// the catalogue.
					// - If these properties are not defined in the component in the BIM file, they
					// will be added because are
					// needed to execute the rule.

					String[] msElementsToCheckProperties_N = array1[1].split(",");
					List<String> msElementsToCheckProperties_N_temp = new ArrayList<String>();

					int liNUm = msElementsToCheckProperties_N.length;
					for (int i = 0; i < liNUm; i++) {
						if ((!msElementsToCheckProperties_N[i].equals(""))
								&& (!msElementsToCheckProperties_N[i].equals(" "))) {
							msElementsToCheckProperties_N_temp
									.add(msElementsToCheckProperties_N[i].replaceAll("\"", "").replaceAll(" ", ""));
						}
					}
					msElementsToCheckProperties_N = msElementsToCheckProperties_N_temp
							.toArray(new String[msElementsToCheckProperties_N_temp.size()]);

					String[][] msElementsToCheckPropertiesCopy = Arrays.copyOf(msComponentsToCheckProperties,
							msComponentsToCheckProperties.length + 1);
					msElementsToCheckPropertiesCopy[msComponentsToCheckProperties.length] = msElementsToCheckProperties_N;
					msComponentsToCheckProperties = msElementsToCheckPropertiesCopy;
				} else if (array1[0].equals("component_" + liNumComponents + "_propertytypes")) {
					String[] msElementsToCheckPropertytypes_N = array1[1].split(",");
					List<String> msElementsToCheckPropertytypes_N_temp = new ArrayList<String>();

					int liNUm = msElementsToCheckPropertytypes_N.length;
					for (int i = 0; i < liNUm; i++) {
						if ((!msElementsToCheckPropertytypes_N[i].equals(""))
								&& (!msElementsToCheckPropertytypes_N[i].equals(" "))) {
							msElementsToCheckPropertytypes_N_temp
									.add(msElementsToCheckPropertytypes_N[i].replaceAll("\"", "").replaceAll(" ", ""));
						}
					}
					msElementsToCheckPropertytypes_N = msElementsToCheckPropertytypes_N_temp
							.toArray(new String[msElementsToCheckPropertytypes_N_temp.size()]);

					String[][] msElementsToCheckPropertytypesCopy = Arrays.copyOf(msComponentsToCheckPropertyTypes,
							msComponentsToCheckPropertyTypes.length + 1);
					msElementsToCheckPropertytypesCopy[msComponentsToCheckPropertyTypes.length] = msElementsToCheckPropertytypes_N;
					msComponentsToCheckPropertyTypes = msElementsToCheckPropertytypesCopy;
				} else if (array1[0].equals("suggestion_product_prefix_uri")) {
					msSuggestionProductPrefixUri = array1[1];
					msSuggestionProductPrefixUri = msSuggestionProductPrefixUri.replaceAll(" ", "");
					msSuggestionProductPrefixUri = msSuggestionProductPrefixUri.replaceAll("\"", "");
				} else if (array1[0].equals("suggestion_product_prefix_name")) {
					msSuggestionProductPrefixName = array1[1];
					msSuggestionProductPrefixName = msSuggestionProductPrefixName.replaceAll(" ", "");
					msSuggestionProductPrefixName = msSuggestionProductPrefixName.replaceAll("\"", "");
				} else if (array1[0].equals("suggestion_product_property")) {
					msSuggestionProductProperty = array1[1];
					msSuggestionProductProperty = msSuggestionProductProperty.replaceAll(" ", "");
					msSuggestionProductProperty = msSuggestionProductProperty.replaceAll("\"", "");
				} else if (array1[0].equals("num_alternatives")) {
					miNumAlternatives = Integer.parseInt(array1[1].replaceAll(" ", ""));
				}
			}
			loBufRead.close();
		} catch (FileNotFoundException fne) {
			System.out.println("Error: File Not Found" + fne);
		} catch (IOException ioe) {
			System.out.println("Error: IOException" + ioe);
		}
	}

	// Method - Retrieve the query
	static String loadSPARQLQueryFile() throws IOException {
		try {
			byte[] loContent = Files.readAllBytes(Paths.get("files/sparql_query.txt"));
			return new String(loContent, Charset.defaultCharset());
		} catch (FileNotFoundException fne) {
			System.out.println("Error: File Not Found" + fne);
		} catch (IOException ioe) {
			System.out.println("Error: IOException" + ioe);
		}
		return "";
	}
}
