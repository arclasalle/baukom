package es.url.salle.arc.chkservice;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.reasoner.rulesys.BindingEnvironment;
import com.hp.hpl.jena.reasoner.rulesys.BuiltinRegistry;
import com.hp.hpl.jena.reasoner.rulesys.RuleContext;
import com.hp.hpl.jena.reasoner.rulesys.Util;
import com.hp.hpl.jena.reasoner.rulesys.builtins.BaseBuiltin;

public class Builtin 
{
	// the default constructor
    public Builtin() 
    {
    	// ************************************************************
    	// SQUARE ROOT
    	// ************************************************************
    	BuiltinRegistry.theRegistry.register( new BaseBuiltin() 
		{
		    @Override
		    public String getName() 
		    {
		        return "sqrt";
		    }
		    @Override
		    public int getArgLength() 
		    {
		    	return 2;
		    }
		    @Override
		    public boolean bodyCall(Node[] args, int length, RuleContext context) 
		    {
		    	try  		    	
		    	{
		    		checkArgs(length, context);
		    	}
		    	catch (Exception e) { e.printStackTrace(); }
		    	
		    	BindingEnvironment env = context.getEnv();
		    	Node n1 = getArg(0, args, context);    	
		    	
		    	if (n1.isLiteral()) 
		    	{
		    		Object v1 = n1.getLiteralValue();    		
		    		Node sqrt = null;
		    		
		    		if (v1 instanceof Number) 
		    		{
		    			Number nv1 = (Number)v1;
		    			if (v1 instanceof Float || v1 instanceof Double) 
		    			{
		    				double sqrtd = Math.sqrt(nv1.doubleValue());
		    				sqrt = Util.makeDoubleNode(sqrtd);
		    			} 
		    			else 
		    			{
		    				long sqrtl = (long) Math.sqrt(nv1.longValue());
		    				sqrt = Util.makeLongNode(sqrtl);
		    			}	
		    			
		    			return env.bind(args[1], sqrt);
		    		}
		    	}
		    	// Doesn't (yet) handle partially bound cases
		    	return false;
		    }
		} );
    	
    	// ************************************************************
    	// POW
    	// ************************************************************
    	BuiltinRegistry.theRegistry.register( new BaseBuiltin() 
		{
		    @Override
		    public String getName() 
		    {
		        return "pow";
		    }
		    @Override
		    public int getArgLength() 
		    {
		    	return 3;
		    }
		    @Override
		    public boolean bodyCall(Node[] args, int length, RuleContext context) 
		    {
		    	try  		    	
		    	{
		    		checkArgs(length, context);
		    	}
		    	catch (Exception e) { e.printStackTrace(); }
		    	
		    	BindingEnvironment env = context.getEnv();
		    	
		    	Node n1 = getArg(0, args, context);
		        Node n2 = getArg(1, args, context);
		         
		        if (n1.isLiteral() && n2.isLiteral()) 
		        {
		        	Object v1 = n1.getLiteralValue();
		        	Object v2 = n2.getLiteralValue();
		             
		        	Node pow = null;
		             
		        	if (v1 instanceof Number && v2 instanceof Number) 
		        	{
		        		Number nv1 = (Number)v1;
		        		Number nv2 = (Number)v2;
		                 
		        		if (v1 instanceof Float || v1 instanceof Double 
		                     || v2 instanceof Float || v2 instanceof Double) 
		        		{
		        			double pwd = Math.pow(nv1.doubleValue(), nv2.doubleValue());
		        			pow = Util.makeDoubleNode(pwd);
		        		} 
		                else 
		                {
		                	long pwd = (long) Math.pow(nv1.longValue(),nv2.longValue());
		                	pow = Util.makeLongNode(pwd);
		                }
		        		return env.bind(args[2], pow);
		             }
		         }
		    	// Doesn't (yet) handle partially bound cases
		    	return false;
		    }
		} );
    	
    	// ************************************************************
    	// ...
    	// ************************************************************

    }	
}
