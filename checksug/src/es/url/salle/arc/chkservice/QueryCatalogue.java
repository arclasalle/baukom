package es.url.salle.arc.chkservice;

/**
 * author: Gon�al Costa (gcosta@salleurl.edu)
 * supervisor: Pieter Pauwels (pipauwel.pauwels@ugent.be)
 * 
 */

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.query.Syntax;

public class QueryCatalogue{
	
	// Members:
	private String msSPARQLEndPointUri = "";
	private String msQuery = "";
	
	private boolean mbTesting = false;
	
	// Method:
	public QueryCatalogue(String asSPARQLEndPointUri, String asQuery) {
		msSPARQLEndPointUri = asSPARQLEndPointUri;
		msQuery = asQuery;
	}
	
	// Method -
	public String[][] getProducts()
	{
		// References:
		// http://blogs.ifgi.de/worldwind-tutorial/linked-open-data/querying-external-triplestores/
		if(!mbTesting){
			String[][] lLstLstProduts = new String[0][0];
			QueryExecution loQueryExecution = null;
			try	{
				Query query = QueryFactory.create(msQuery, Syntax.defaultQuerySyntax);
			        
				loQueryExecution = QueryExecutionFactory.sparqlService(msSPARQLEndPointUri, query);
				ResultSet loResults = loQueryExecution.execSelect();
				
				List<QuerySolution> llstQuerySolutions = ResultSetFormatter.toList(loResults);
				
				int i = 0;
				List<String> llstResultVars = loResults.getResultVars();
				while (i < llstQuerySolutions.size()) 
				{
					int v = 0;
					QuerySolution loQuerySolution = llstQuerySolutions.get(i);
					String[] laInfoProduct = new String[llstResultVars.size()];
					while (v < llstResultVars.size()) 
					{
						RDFNode loVariable = loQuerySolution.get(llstResultVars.get(v));
						if(loVariable.isLiteral())
						{
							// Remove the
							String lsLiteralTemp = loVariable.toString();
							if(lsLiteralTemp.contains("^^")){
								lsLiteralTemp = lsLiteralTemp.split("\\^\\^")[0];
							}
							laInfoProduct[v] = lsLiteralTemp;
						}
						else{
							laInfoProduct[v] = loVariable.toString();
						}
						v++;
					}
        	    	
        	    	String[][] mslLstLstProdutsCopy = Arrays.copyOf(lLstLstProduts, lLstLstProduts.length+1);
        	    	mslLstLstProdutsCopy[lLstLstProduts.length] = laInfoProduct;
        	    	lLstLstProduts= mslLstLstProdutsCopy;
					i++;
				}
				
				loQueryExecution.close();
			}
			catch(Throwable e) 
			{
				System.out.println("Error: impossible to execute the SPARQL query to retrieve the products of the catalogue.");
				if(loQueryExecution!=null){
					loQueryExecution.close();
				}
			}
			 
			return lLstLstProduts;
		}
		else
		{
			// Test data!			
			// HARCODED info from PRECAT S.L. Company products
			String[][] lLstLstProdutsTest = 
				{
					// height (m) 	Weight (kn/m)   Uri
					{"0.16",        "2.695", 	"http://www.baukomcatalogue.org/companies/precat/products/LP16"},   		//LP-16
			        {"0.20",        "2.891",    "http://www.baukomcatalogue.org/companies/precat/products/LP20"},   		//LP-20
			        {"0.25",        "3.577",    "http://www.baukomcatalogue.org/companies/precat/products/LP25"},   		//LP-25
			        {"0.32",        "4.508",    "http://www.baukomcatalogue.org/companies/precat/products/LP32"},   		//LP-32
			        {"0.40",        "5.0813",   "http://www.baukomcatalogue.org/companies/precat/products/LP40"},   		//LP-40
			        {"0.50",        "7.4235",   "http://www.baukomcatalogue.org/companies/precat/products/LP50"},   		//LP-50
			        {"0.63",        "9.6775",   "http://www.baukomcatalogue.org/companies/precat/products/LP63"},   		//LP-63
			        {"0.83",        "11.3092",  "http://www.baukomcatalogue.org/companies/precat/products/LP83"} 			//LP-83
				};
			
			return lLstLstProdutsTest;
		}
	}
	
	// Method -
	public boolean isDefined(	Model aoModelBIM,
								Resource aoSubject,									   
								Property aoProperty)
	{
		NodeIterator it = aoModelBIM.listObjectsOfProperty(aoSubject, aoProperty);
		if (it.hasNext())
			return true;	
		return false;
	}
}
